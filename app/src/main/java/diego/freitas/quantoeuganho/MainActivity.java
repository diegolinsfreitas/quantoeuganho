package diego.freitas.quantoeuganho;

import diego.freitas.quantoeuganho.util.NetworkUtils;
import im.delight.apprater.AppRater;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.UserHandle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.appinvite.AppInviteInvitationResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.PlusOneButton;

import diego.freitas.quantoeuganho.util.FontUtils;

public class MainActivity extends FragmentActivity implements OnClickListener, com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener{

	
	public static final int TUTORIAL_REQUEST = 10;
	private static final String DISCLAIMER_ACCEPTED = "disclaimer_accepted";
	private DisclaimerDialogFragment disclaimerDialogFragment;
	private RemoveAdsDialogFragment removeAdsFragment;
	private AdService adService;
	private PreferencesManager preferencesManager;
	private boolean mShowDialog;
	
	private static final int REQUEST_INVITE = 0;
	protected static final String TAG = MainActivity.class.getName();

	private GoogleApiClient mGoogleApiClient;
	private String service;
	private PlusOneButton mPlusOneButton;
	private static final int PLUS_ONE_REQUEST_CODE = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		FontUtils.overrideFonts(this, FontUtils.ROBOTO_LIGHT,
				findViewById(R.id.main_activity_container));

		//adRemoval = new AdRemoval(this);

		//mPlusOneButton = (PlusOneButton) findViewById(R.id.plus_one_button);

		if (NetworkUtils.isOnline()) {
			/*AppRater appRater = new AppRater(this);
			appRater.setLaunchesBeforePrompt(2);
			appRater.setDaysBeforePrompt(2);
			appRater.setPhrases(R.string.rate_title, R.string.rate_explanation,
					R.string.rate_now, R.string.rate_later, R.string.rate_never);
			appRater.show();*/
		}

		adService = new AdService(this);
		adService.loadAd(this);


		preferencesManager = new PreferencesManager(this);
		//Toast.makeText(MainActivity.this, "STATUS PROPAGANDA "+ preferencesManager.ads(), Toast.LENGTH_LONG).show();

		//if(adService.ADS_ENABLE){
		//	((Button) findViewById(R.id.invite_button)).setText("Remover propaganda grátis");
		//} else {
			((Button) findViewById(R.id.invite_button)).setText("Indique o aplicativo");
		//}
		
		// Invite button click listener
        findViewById(R.id.invite_button).setOnClickListener(this);
        // [END_EXCLUDE]

        // Create an auto-managed GoogleApiClient with acccess to App Invites.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(AppInvite.API)
                .addApi(AppIndex.API)
                //.addApi(Plus.API)
                .enableAutoManage(this, this)
                .build();

        // Check for App Invite invitations and launch deep-link activity if possible.
        // Requires that an Activity is registered in AndroidManifest.xml to handle
        // deep-link URLs.
        boolean autoLaunchDeepLink = true;
        AppInvite.AppInviteApi.getInvitation(mGoogleApiClient, this, autoLaunchDeepLink)
                .setResultCallback(
                        new ResultCallback<AppInviteInvitationResult>() {
                            @Override
                            public void onResult(AppInviteInvitationResult result) {
                            	if(result != null && result.getStatus().isSuccess()){
                            		QuantoEuGanhoApplication.gaTracker.sendInviteConversion();
                            	}
                            }
                        });
	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//mPlusOneButton.initialize("http://market.android.com/details?id=diego.freitas.quantoeuganho", PLUS_ONE_REQUEST_CODE);
		if (hasNotAcceptedDisclaimer()) {
			showDisclaimerDialog();
		}
		adService.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		adService.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		adService.onDestroy();
	}
	
	static final Uri APP_URI = Uri.parse("android-app://diego.freitas.quantoeuganho/http/salarioliquidosaldofgts.com.br/");
	static final Uri WEB_URL = Uri.parse("http://salarioliquidosaldofgts.com.br/");
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mGoogleApiClient.connect();
		String title = "Saldo FGTS Salario Liquido Decimo terceiro";

		// Construct the Action performed by the user
		Action viewAction = Action.newAction(Action.TYPE_VIEW, title, WEB_URL, APP_URI);

		// Call the App Indexing API start method after the view has completely rendered
		AppIndex.AppIndexApi.start(mGoogleApiClient, viewAction);
	}

	private boolean hasNotAcceptedDisclaimer() {
		SharedPreferences preferences = getPreferences(MODE_PRIVATE);
		return !preferences.getBoolean(DISCLAIMER_ACCEPTED, false);
	}

	private void showDisclaimerDialog() {
		Fragment prev = getSupportFragmentManager().findFragmentByTag(
				DisclaimerDialogFragment.class.getName());
		if (prev == null) {
			disclaimerDialogFragment = new DisclaimerDialogFragment();
			disclaimerDialogFragment.aggrededListener = this;
			disclaimerDialogFragment.rejectListener = this;
			disclaimerDialogFragment.show(getSupportFragmentManager(),
					DisclaimerDialogFragment.class.getName());
		}
	}
	
	private void showRemoveAdsDialog() {
		Fragment prev = getSupportFragmentManager().findFragmentByTag(
				DisclaimerDialogFragment.class.getName());
		if (prev == null) {
			removeAdsFragment = new RemoveAdsDialogFragment();
			removeAdsFragment.aggrededListener = new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					MainActivity.this.onInviteClicked();
					
				}
			};
			removeAdsFragment.show(getSupportFragmentManager(),
					RemoveAdsDialogFragment.class.getName());
		}
	}

	public void onMenuSalaryClick(View v) {
		startActivity(new Intent(this, SalarioLiquidoActivity.class));
	}

	public void onMenuFgtsClicked(View v) {
		startActivity(new Intent(this, SaldoFgtsActivity.class));
	}

	public void onMenuDecimoTerceiroClick(View v) {
		startActivity(new Intent(this, DecimoTerceiroActivity.class));
	}

	public void onMenuHistoryClicked(View v) {
		startActivity(new Intent(this, HistoryActivity.class));
	}
	
	public void onMenuSeguroDesempregoClicked(View v) {
		if (preferencesManager.understoodTutorial()) {
			LoginFGTSDialogFragment fgtsDialogFragment = new LoginFGTSDialogFragment();
			Bundle bundle = new Bundle();
			bundle.putString("service", "seguro");
			fgtsDialogFragment.setArguments(bundle);
			fgtsDialogFragment.show(getSupportFragmentManager(),
					LoginFGTSDialogFragment.class.getName());
			
		} else {
			Intent intent = new Intent(this, TutorialNisActivity.class);
			intent.putExtra("service", "seguro");
			startActivityForResult(intent,
					TUTORIAL_REQUEST);
		}
	}
	

	public void onMenuExtratoFgtsClicked(View v) {
		if (preferencesManager.understoodTutorial()) {
			LoginFGTSDialogFragment fgtsDialogFragment = new LoginFGTSDialogFragment();
			Bundle bundle = new Bundle();
			bundle.putString("service", "fgts");
			fgtsDialogFragment.setArguments(bundle);
			fgtsDialogFragment.show(getSupportFragmentManager(),
					LoginFGTSDialogFragment.class.getName());
		} else {
			Intent intent = new Intent(this, TutorialNisActivity.class);
			intent.putExtra("service", "fgts");
			startActivityForResult(intent,
					TUTORIAL_REQUEST);
		}
	}

	@Override
	protected void onActivityResult(int type, int result, Intent data) {
		super.onActivityResult(type, result, data);
		Log.d(TAG, "onActivityResult: requestCode=" + type + ", resultCode=" + result);
		if (type == TUTORIAL_REQUEST && result == Activity.RESULT_OK) {
			mShowDialog = true;
			service = data.getStringExtra("service");
		}
		
        if (type == REQUEST_INVITE) {
            if (result == RESULT_OK) {
                // Check how many invitations were sent and log a message
                // The ids array contains the unique invitation ids for each invitation sent
                // (one for each contact select by the user). You can use these for analytics
                // as the ID will be consistent on the sending and receiving devices.
                String[] ids = AppInviteInvitation.getInvitationIds(result, data);
                QuantoEuGanhoApplication.gaTracker.invites(adService.inviteGroup(), ids);
                
                preferencesManager.invites(ids.length);
                /*if(AdService.ADS_ENABLE){
	                if(preferencesManager.invites() >= adService.minInvites()){
	                	removeAds();
	                	showMessage("Propaganda Removida. Obrigado");
	                } else {
	                	showMessage("Ainda faltam " + (adService.minInvites() - ids.length) +" convites");
	                }
                }*/
                //Log.d(TAG, getString(R.string.sent_invitations_fmt, ids.length));
            } else {
                // Sending failed or it was canceled, show failure message to the user
               // showMessage("Erro ao enviar convites");
            }
        }
	}

	private void removeAds() {
		preferencesManager.removeAds();
		adService.ADS_ENABLE = preferencesManager.ads();
		findViewById(R.id.adView).setVisibility(View.GONE);
		((Button) findViewById(R.id.invite_button)).setText("Indique o aplicativo");
	}

	@Override
	protected void onResumeFragments() {
		super.onResumeFragments();

		// play with fragments here
		if (mShowDialog) {
			mShowDialog = false;

			// Show only if is necessary, otherwise FragmentManager will take
			// care
			if (getSupportFragmentManager().findFragmentByTag(
					LoginFGTSDialogFragment.class.getName()) == null) {
				LoginFGTSDialogFragment fgtsDialogFragment = new LoginFGTSDialogFragment();
				Bundle bundle = new Bundle();
				bundle.putString("service", service);
				fgtsDialogFragment.setArguments(bundle);
				fgtsDialogFragment.show(getSupportFragmentManager(),
						LoginFGTSDialogFragment.class.getName());
			}
		}
	}

	public void onMenuHelpClicked(View v) {

		AboutDialogFragment aboutDialog = new AboutDialogFragment();

		aboutDialog.show(getSupportFragmentManager(),
				AboutDialogFragment.class.getName());
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_agreed:
			saveDisclaimerAcceptance();
			break;
		 case R.id.invite_button:
			 //if(AdService.ADS_ENABLE){
			//	 showRemoveAdsDialog();
			// } else {
				 onInviteClicked();
			// }
             break;
		default:
			break;
		}
	}

	private void saveDisclaimerAcceptance() {
		SharedPreferences preferences = getPreferences(MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putBoolean(DISCLAIMER_ACCEPTED, true);
		editor.commit();
		disclaimerDialogFragment.dismiss();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Log.d(TAG, "onConnectionFailed:" + result);
        //showMessage("");
	}
	

    /**
     * User has clicked the 'Invite' button, launch the invitation UI with the proper
     * title, message, and deep link
     */
    // [START on_invite_clicked]
    private void onInviteClicked() {
		//removeAds();
    	try {
	        Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
	                .setMessage(getString(R.string.invitation_message))
	                .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
	                .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
	                .setCallToActionText(getString(R.string.invitation_cta))
	                .build();
	        startActivityForResult(intent, REQUEST_INVITE);
    	} catch (Exception  ac ) {
            inviteFallback();
			//removeAds();
        }
    	//removeAdsFragment.dismiss();
    }

	private void inviteFallback(){
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, "Gostei deste aplicativo pra consultar saldo fgts e cálculos trabalhistas. http://goo.gl/XsPgCY");
		sendIntent.setType("text/plain");
		startActivity(sendIntent);
	}
    // [END on_invite_clicked]

	private void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}
