package diego.freitas.quantoeuganho;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import diego.freitas.quantoeuganho.fgts.Evento;
import diego.freitas.quantoeuganho.util.FontUtils;

public class EventosAdapter extends BaseAdapter {

	List<Evento> data;
	LayoutInflater layoutInflater;
	public Activity parentActivity;

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return data.get(position).hashCode();
	}

	@Override
	public View getView(int index, View v, ViewGroup vGroup) {
		ViewHolder viewHolder;
		if (v == null) {
			v = layoutInflater.inflate(R.layout.evento_item, vGroup, false);
			FontUtils.overrideFonts(layoutInflater.getContext(),
					FontUtils.ROBOTO_LIGHT, v);
			viewHolder = new ViewHolder();
			viewHolder.txvParcela = (TextView) v.findViewById(R.id.txv_parcela);
			viewHolder.txvValor = (TextView) v.findViewById(R.id.txv_valor);
			viewHolder.txvPrazo = (TextView) v.findViewById(R.id.txv_prazo);
			viewHolder.txvStatus = (TextView) v.findViewById(R.id.txv_status);
			v.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) v.getTag();
		}

		final Evento evento = (Evento) getItem(index);
		if (evento != null) {
			loadView(viewHolder, evento, index);
		}
		return v;
	}

	private void loadView(ViewHolder viewHolder, final Evento evento,
			final int index) {
		viewHolder.txvParcela.setText(evento.parcela);
		viewHolder.txvValor.setText(evento.valor);
		viewHolder.txvPrazo.setText(evento.prazo);
		viewHolder.txvStatus.setText(evento.situacao);
		 
	}

	static class ViewHolder {

		public TextView txvStatus;
		public TextView txvPrazo;
		public TextView txvValor;
		public TextView txvParcela;

	}

	@Override
	public boolean isEmpty() {
		return data.isEmpty();
	}

}
