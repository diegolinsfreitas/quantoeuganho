package diego.freitas.quantoeuganho;

import diego.freitas.quantoeuganho.R;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

class MenuDelegate {

	private FragmentActivity act;

	public MenuDelegate(FragmentActivity act) {
		this.act = act;
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = act.getMenuInflater();
		inflater.inflate(R.menu.qeg_menu, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_help:
			AboutDialogFragment aboutDialog = new AboutDialogFragment();

			aboutDialog.show(act.getSupportFragmentManager(),
					AboutDialogFragment.class.getName());
			break;

		default:
			break;
		}

		return true;
	}
}