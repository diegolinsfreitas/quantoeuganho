package diego.freitas.quantoeuganho;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import diego.freitas.quantoeuganho.fgts.Crawler;
import diego.freitas.quantoeuganho.fgts.CrawlerException;
import diego.freitas.quantoeuganho.fgts.DownloadDataService;
import diego.freitas.quantoeuganho.fgts.Seguro;
import diego.freitas.quantoeuganho.model.ContaFGTS;
import diego.freitas.quantoeuganho.util.FontUtils;
import diego.freitas.quantoeuganho.util.NetworkUtils;

public class LoginFGTSDialogFragment extends DialogFragment {
	
	private EditText nis;
	private EditText pass;
	private PreferencesManager preferencesManager;
	private TextView message;
	private View btnLogin;
	
	private AsyncTask<String, Void, Object> execute;
	private Button btnGetPass;
    private DownloadStateReceiver mDownloadStateReceiver;
    private String error = "Não foi possível consultar dos dados!";
    private String service;

    @Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	  Dialog dialog = super.onCreateDialog(savedInstanceState);
	  dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
	  dialog.setCanceledOnTouchOutside(false);
	  
	  return dialog;
	}



    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		preferencesManager = new PreferencesManager(getActivity());
		View v = inflater.inflate(R.layout.login_fgts, container, false);
		FontUtils.overrideFonts(getActivity(), FontUtils.ROBOTO_LIGHT, v);
		nis = (EditText) v.findViewById(R.id.edt_nis);
		pass = (EditText) v.findViewById(R.id.edt_pass);
		message = (TextView) v.findViewById(R.id.txt_message);
		btnGetPass = (Button) v.findViewById(R.id.btn_get_password);
		
		service = getArguments().getString("service");
		btnGetPass.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginFGTSDialogFragment.this.getActivity(), TutorialNisActivity.class);
                intent.putExtra("service", service);
                startActivity(intent);
            }
        });

        if(preferencesManager.getPass() != null ) {
            btnGetPass.setVisibility(View.GONE);
        }
		
		nis.setText(preferencesManager.getNis());
		pass.setText(preferencesManager.getPass());
		btnLogin = v.findViewById(R.id.btn_login);
		
		btnLogin.setOnClickListener(

                new OnClickListener() {


                    public void onClick(View v) {
                        if (!NetworkUtils.isOnline()) {
                            setErrorMessage("Verifique sua conexão de internet");
                            return;
                        }
                        String nisNumber = nis.getText().toString();
                        String password = pass.getText().toString();
                        preferencesManager.saveLogin(nisNumber, password);
                        Intent mServiceIntent = new Intent(getContext().getApplicationContext(), DownloadDataService.class);
                        mServiceIntent.putExtra("nis", nisNumber);
                        mServiceIntent.putExtra("password", password);
                        mServiceIntent.putExtra("service", service);
                        getActivity().startService(mServiceIntent);
                        onPreExecute();
                    }
                });

		return v;
	}

    protected void onPreExecute() {
        disable();
        setMessage("Consultando dados...");
    }

    protected void onCancelled() {
        enable();
        if (!LoginFGTSDialogFragment.this.isDetached() && LoginFGTSDialogFragment.this.isVisible()) {
            setErrorMessage("Consulta cancelada, Tente novamente");
        }
    }

    protected void onPostExecute(Object result) {
        enable();
        if (result == null) {
            setErrorMessage(this.error);

            Intent mServiceIntent = new Intent(getContext().getApplicationContext(), DownloadDataService.class);
            mServiceIntent.putExtra("nis", preferencesManager.getNis());
            mServiceIntent.putExtra("password", preferencesManager.getPass());
            mServiceIntent.putExtra("service", service);
            mServiceIntent.putExtra("background", true);

            PendingIntent pintent = PendingIntent.getService(getContext().getApplicationContext(), 0, mServiceIntent, 0);
            AlarmManager alarm = (AlarmManager)this.getContext().getSystemService(Context.ALARM_SERVICE);
            Random rn = new Random();
            alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), 10 + rn.nextInt(20 - 15 + 1) * 60 * 1000, pintent);

        } else if ("fgts".equals(service)) {
            Intent intent = new Intent(LoginFGTSDialogFragment.this.getActivity(), ExtratoFGTSActivity.class);
            intent.putExtra("contas", (ArrayList<ContaFGTS>) result);
            startActivity(intent);
            LoginFGTSDialogFragment.this.dismissAllowingStateLoss();
        } else if ("seguro".equals(service)) {
            Intent intent = new Intent(LoginFGTSDialogFragment.this.getActivity(), SeguroDesempregoActivity.class);
            intent.putExtra("seguro", (HashMap) result);
            startActivity(intent);
            LoginFGTSDialogFragment.this.dismissAllowingStateLoss();
        }
    }


	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	private void enable(){
		nis.setEnabled(true);
		pass.setEnabled(true);
		btnLogin.setEnabled(true);
	}
	
	private void setMessage(String message){
		this.message.setText(message);
		this.message.setTextColor(getResources().getColor(R.color.blue11));
	}
	
	private void setErrorMessage(String message){
		this.message.setText(message);
        this.message.setTextColor(getResources().getColor(R.color.red));
        btnGetPass.setVisibility(View.GONE);
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
		// TODO Auto-generated method stub
		super.onCancel(dialog);
		//TODO Send braod cast to cancel service
	}
	
	private void disable(){
		nis.setEnabled(false);
		pass.setEnabled(false);
		btnLogin.setEnabled(false);
	}

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(
                mDownloadStateReceiver);
    }

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter statusIntentFilter = new IntentFilter(
                DownloadDataService.BROADCAST_ACTION);

        mDownloadStateReceiver =
                new DownloadStateReceiver();

        LocalBroadcastManager.getInstance(this.getContext().getApplicationContext()).registerReceiver(
                mDownloadStateReceiver, statusIntentFilter);
    }



    private class DownloadStateReceiver extends BroadcastReceiver
    {
        // Prevents instantiation
        private DownloadStateReceiver() {
        }
        // Called when the BroadcastReceiver gets an Intent it's registered to receive
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("category").equals(DownloadDataService.ERROR_CATEGORY)){
                LoginFGTSDialogFragment.this.error = intent.getStringExtra("error") +"\n Iremos avisar quando voltar ao normal pelo aplicativo";
                onPostExecute(null);
            } else if(intent.getStringExtra("category").equals(DownloadDataService.RESULT_CATEGORY)){
                onPostExecute(intent.getSerializableExtra("data"));
            }
        }
    }

}
