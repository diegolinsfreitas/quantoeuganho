package diego.freitas.quantoeuganho.fgts;

import java.io.Serializable;

public class Evento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String parcela;
	public String valor;
	public String prazo;
	public String situacao;
	public Evento(String parcela, String valor, String prazo,
			String situacao) {
		super();
		this.parcela = parcela;
		this.valor = valor;
		this.prazo = prazo;
		this.situacao = situacao;
	}
}