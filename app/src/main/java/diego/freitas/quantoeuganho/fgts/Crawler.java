package diego.freitas.quantoeuganho.fgts;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import diego.freitas.quantoeuganho.PreferencesManager;
import diego.freitas.quantoeuganho.model.ContaFGTS;
import diego.freitas.quantoeuganho.model.CreditoFGTS;
import diego.freitas.quantoeuganho.util.NumberUtils;

public class Crawler {

	public static final String HTTPS_LOGIN = "https://servicossociais.caixa.gov.br/internet-segmento-cidadao.do";
	public static final String HTTPS_MENU = "https://extratofgts.caixa.gov.br/SIBNS/menu_extrato.processa";
	public static final String HTTPS_NAVIGATE_SALDOS = "https://extratofgts.caixa.gov.br/SIBNS/menu_extrato_sisgr.processa?req=C&opcao=X&novo=S";
	public static final String HTTPS_PROCESSA = "https://extratofgts.caixa.gov.br/SIBNS/menu_extrato_sisgr.processa?req=C&opcao=M";
	public static final String END_FORM = "</form>";
	private final Context context;
	private final PreferencesManager preferencesManager;

	private HttpClient httpClient;
	private HttpContext httpContext;
	public ArrayList<ContaFGTS> contas;
	public HashMap<String, Seguro> seguros = new HashMap<>();
	
	public Crawler(Context context) throws CrawlerException{
		this.contas = new ArrayList<ContaFGTS>();
		/*if(!AdService.ADS_ENABLE){
			buildFakeData();
		}*/

		preferencesManager = new PreferencesManager(context);
		this.context = context;
		AssetManager assetManager = context.getAssets();
		try {
			InputStream ims = assetManager.open("ICP-Brasil v2.cer");
			buildClient(ims);
		} catch (IOException e) {
			throw new CrawlerException("ERRO: CERT001. Erro Interno do aplicativo avise o desenvolvedor");
		}


		
	}

	public  void buildClient(InputStream ims) {
		KeyStore keyStore;
		DefaultHttpClient client;
		try {
			keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			keyStore.load(null, null);
			SSLSocketFactory sslSocketFactory = new SSLSocketFactory(keyStore, ims);
			sslSocketFactory
					.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			BasicHttpParams localBasicHttpParams = new BasicHttpParams();
			HttpProtocolParams.setVersion(localBasicHttpParams,
					HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(localBasicHttpParams, "UTF-8");
			SchemeRegistry registry = new SchemeRegistry();
			Scheme httpScheme = new Scheme("http",
					PlainSocketFactory.getSocketFactory(), 80);
			registry.register(httpScheme);
			Scheme schemeHttps = new Scheme("https", sslSocketFactory, 443);
			registry.register(schemeHttps);
			ThreadSafeClientConnManager clientConnManager = new ThreadSafeClientConnManager(
					localBasicHttpParams, registry);
			client = new DefaultHttpClient(clientConnManager,
					localBasicHttpParams);
			
			CookieStore cookieStore = new BasicCookieStore();
			httpContext = new BasicHttpContext();
			httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			client = new DefaultHttpClient();
		}
		this.httpClient = client;
        this.httpClient.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
	}

	public void buildFakeData(){
		ContaFGTS contaFGTS = new ContaFGTS();
		contaFGTS.accountNumber = "00004837392";
		contaFGTS.empresa = "Ambev SA";
		contaFGTS.saldo = "R$20.922,89";
		contaFGTS.creditos.add(new CreditoFGTS("5/02/2015", "115-DEPOSITO JANEIRO/2015", "R$200,00"));
		contaFGTS.creditos.add(new CreditoFGTS("10/02/2015", "CREDITO DE JAM 0,003542", "R$ 4,54"));
		contaFGTS.creditos.add(new CreditoFGTS("5/03/2015", "115-DEPOSITO FEVEREIRO/2015", "R$ 200,00"));
		contaFGTS.creditos.add(new CreditoFGTS("10/03/2015", "CREDITO DE JAM 0,003542", "R$ 4,64"));
		contaFGTS.creditos.add(new CreditoFGTS("5/04/2015", "115-DEPOSITO MARCO/2015", "R$ 200,00"));
		contaFGTS.creditos.add(new CreditoFGTS("10/04/2015", "CREDITO DE JAM 0,003542", "R$ 4,74"));
		contaFGTS.creditos.add(new CreditoFGTS("5/05/2015", "115-DEPOSITO ABRIL/2015", "R$ 200,00"));
		contaFGTS.creditos.add(new CreditoFGTS("10/05/2015", "CREDITO DE JAM 0,003542", "R$ 4,84"));
		contaFGTS.creditos.add(new CreditoFGTS("5/06/2015", "115-DEPOSITO MAIO/2015", "R$ 200,00"));
		contaFGTS.creditos.add(new CreditoFGTS("10/06/2015", "CREDITO DE JAM 0,003542", "R$ 4,94"));
		contaFGTS.creditos.add(new CreditoFGTS("5/07/2015", "115-DEPOSITO JUNHO/2015", "R$ 200,00"));
		contaFGTS.creditos.add(new CreditoFGTS("10/07/2015", "CREDITO DE JAM 0,003542", "R$ 5,04"));
		
		contas.add(contaFGTS);
		
		contaFGTS = new ContaFGTS();
		contaFGTS.accountNumber = "00004837392";
		contaFGTS.empresa = "Grendene SA";
		contaFGTS.saldo = "R$10.989,89";
		contas.add(contaFGTS);
		
		contaFGTS = new ContaFGTS();
		contaFGTS.accountNumber = "00004837392";
		contaFGTS.empresa = "Cielo";
		contaFGTS.saldo = "R$53.092,89";
		//contas.add(contaFGTS);
		
		/*seguro = new Seguro();
		seguro.nRequerimento = "4.935.277332-6";
		seguro.nome = "João Ancântara Mendonça";
		seguro.totalParcelas = "2";
		seguro.eventos = new ArrayList<Evento>();
		seguro.eventos.add(new Evento("01", "R$ 790,00","","PAGA EM 01/01/2015, AGENCIA BRASIL, SP - SP"));
		seguro.eventos.add(new Evento("02", "R$ 790,00","","PAGA EM 01/02/2015, AGENCIA BRASIL, SP - SP"));*/
	}
	
	public void carregarExtrado(String user, String password) throws CrawlerException {
		try {
			ArrayList<BasicNameValuePair> localArrayList;
			final HttpResponse execute = performLogin(user, password);

			if(execute.getStatusLine().getStatusCode() == 200){
				Document document = validateLogin(execute);

				localArrayList = loadEnvParams(document,"SFG");


				HttpPost processaExtratoPost = new HttpPost(HTTPS_MENU);
				processaExtratoPost.addHeader(
						"User-Agent",
						"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");
				UrlEncodedFormEntity entityProcessaExtrato = null;

				entityProcessaExtrato = new UrlEncodedFormEntity(localArrayList);

				processaExtratoPost.setEntity(entityProcessaExtrato);
				HttpResponse procExtratoResponse = null;
				try {
					procExtratoResponse = this.httpClient.execute(processaExtratoPost, this.httpContext);
				} catch (Exception e) {
					if(e.getMessage().contains("refused")){
						throw new CrawlerException("O serviço de fgts está indisponível no momento. Tente mais tarde");
					}
				}


				String html = EntityUtils.toString(procExtratoResponse.getEntity());
				Element formRedirect = Jsoup.parse(html).getElementsByAttributeValue("name", "frmExec_menu_extrato").get(0);
				if(formRedirect == null){
					throw new CrawlerException("Não foi possível ler os dados  do extrato");
				}


				HttpGet extratoGet = new HttpGet(HTTPS_PROCESSA);
				extratoGet.addHeader(
						"User-Agent",
						"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");
				HttpResponse extratoResponse = this.httpClient.execute(extratoGet, this.httpContext);
				Document extratoDoc = Jsoup.parse(EntityUtils.toString(extratoResponse.getEntity()));

				int totalPages = Integer.valueOf(extratoDoc.getElementsContainingOwnText("Extrato:").get(0).html().split("/")[1]);

				parseFgtsData(extratoDoc);

				for (int i = 1; i < totalPages; i++) {

					HttpPost extratoPost = new HttpPost(HTTPS_NAVIGATE_SALDOS);
					extratoPost.addHeader(
							"User-Agent",
							"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");
					ArrayList<BasicNameValuePair> paramsFormExtrato = new ArrayList<BasicNameValuePair>();
					paramsFormExtrato.add(new BasicNameValuePair("hdnAction", "ret_extrato"));
					paramsFormExtrato.add(new BasicNameValuePair("indicePagina", String.valueOf(i)));

					UrlEncodedFormEntity formIndic = new UrlEncodedFormEntity(paramsFormExtrato);
					extratoPost.setEntity(formIndic);
					extratoResponse = this.httpClient.execute(extratoPost, this.httpContext);
					extratoDoc = Jsoup.parse(EntityUtils.toString(extratoResponse.getEntity()));
					parseFgtsData(extratoDoc);

				}
			}
		} catch (UnsupportedEncodingException e) {
			Log.e("CRAWLER", "carregarExtrado", e);
			throw new CrawlerException();
		} catch (ClientProtocolException e) {
			Log.e("CRAWLER", "carregarExtrado", e);
			throw new CrawlerException();
		} catch (IOException e) {
			Log.e("CRAWLER", "carregarExtrado", e);
			throw new CrawlerException();
		} catch (Exception e) {
			throw new CrawlerException();
		}

		seriralizeData("fgts", contas);
	}

	/*
	 * By default load params from the first page opened
	 */
	private ArrayList<BasicNameValuePair> loadEnvParams(Document document, String system) {
		Element formProcessa = document.getElementById("menu_extrato.processa");
		
		ArrayList<BasicNameValuePair> localArrayList = formToNamveValuePairs(formProcessa);
		
		if("SDE".equals(system)){
			ArrayList<BasicNameValuePair> segDesParamsn = new ArrayList<BasicNameValuePair>();
			for (Iterator<BasicNameValuePair> iterator = localArrayList.iterator(); iterator
					.hasNext();) {
				BasicNameValuePair basicNameValuePair = (BasicNameValuePair) iterator
						.next();
				BasicNameValuePair newValue = null;
				if(basicNameValuePair.getName().equals("SGR_ESTAGIO_AMBIENTE")){
					newValue = new BasicNameValuePair(basicNameValuePair.getName(), "SDE");
					
				} else if(basicNameValuePair.getName().equals("SGR_SISTEMA")){
					newValue = new BasicNameValuePair(basicNameValuePair.getName(), "SDE");
					
				}else if(basicNameValuePair.getName().equals("SGR_PROCESSO")){
					newValue = new BasicNameValuePair(basicNameValuePair.getName(), "S01");
					
				}
				
				if(newValue != null){
					segDesParamsn.add(newValue);
				} else {
					segDesParamsn.add(basicNameValuePair);
				}
				
			}
			localArrayList = segDesParamsn;
		}
		return localArrayList;
	}

	private ArrayList<BasicNameValuePair> formToNamveValuePairs(Element formProcessa) {
		ArrayList<BasicNameValuePair> localArrayList  = new ArrayList<BasicNameValuePair>();

		Iterator<Element> soupIterator = formProcessa.select("input")
				.iterator();
		while (soupIterator.hasNext()) {
			Element localElement = (Element) soupIterator.next();
			if (!localElement.attr("id").equals("")) {
				localArrayList.add(new BasicNameValuePair(localElement
						.attr("id"), localElement.attr("value")));
			}
		}
		return localArrayList;
	}

	private Document validateLogin(final HttpResponse execute) throws  CrawlerException {
		String mainPageContent = null;
		try {
			mainPageContent = EntityUtils.toString(execute.getEntity());
		} catch (IOException e) {
			Log.e("CRAWLER","READ MAIN PAGE CONTENT", e);
			throw new CrawlerException();
		}
		if(mainPageContent.contains("id='SFG'")) {
			int indexFormProcessa = mainPageContent.indexOf("<form action=\"\" id=\"menu_extrato.processa\"");
			mainPageContent = mainPageContent.substring(indexFormProcessa);
			int indexEndFormProcessa = mainPageContent.indexOf(END_FORM)+END_FORM.length();
			return Jsoup.parse(mainPageContent.substring(0, indexEndFormProcessa));
		} else {
			Document document = Jsoup.parse(mainPageContent);
			Element messageElement = document.getElementById("mensagem");
			throw new CrawlerException(messageElement.val());
		}
	}

	private HttpResponse performLogin(String user, String password) throws CrawlerException {
		try {
			ArrayList<BasicNameValuePair> localArrayList = new ArrayList<BasicNameValuePair>();
			localArrayList.add(new BasicNameValuePair("erroJCaptcha", ""));
			localArrayList.add(new BasicNameValuePair("metodo", ""));
			localArrayList.add(new BasicNameValuePair("tipoRequisicao",
					"verificaNivel"));
			localArrayList.add(new BasicNameValuePair("siglaAmbiente", "C0000001"));
			localArrayList.add(new BasicNameValuePair("ambiente", "internet"));
			localArrayList.add(new BasicNameValuePair("segmento", "CIDADAO"));
			localArrayList.add(new BasicNameValuePair("sistema", ""));
			localArrayList.add(new BasicNameValuePair("rxcapcha", ""));
			localArrayList.add(new BasicNameValuePair("login", ""));
			localArrayList.add(new BasicNameValuePair("esqueci", ""));
			localArrayList.add(new BasicNameValuePair("emailnovo", ""));
			localArrayList.add(new BasicNameValuePair("compemail", ""));
			localArrayList.add(new BasicNameValuePair("snhCredencial", password));
			localArrayList.add(new BasicNameValuePair("contador", ""));
			localArrayList.add(new BasicNameValuePair("acaoAction",
					"internet-segmento-cidadao"));
			localArrayList.add(new BasicNameValuePair("focus", ""));
			localArrayList.add(new BasicNameValuePair("exibeCap", "xxxx"));
			localArrayList.add(new BasicNameValuePair("produto", ""));
			localArrayList.add(new BasicNameValuePair("usuario", user));
			localArrayList.add(new BasicNameValuePair("senha", password));

			final HttpPost urlPostLogin = new HttpPost(HTTPS_LOGIN);
			urlPostLogin.addHeader(
					"User-Agent",
					"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");
			final UrlEncodedFormEntity entity = new UrlEncodedFormEntity(localArrayList);
			urlPostLogin.setEntity(entity);
			final HttpResponse execute = this.httpClient.execute(urlPostLogin, this.httpContext);
			return execute;
		} catch (UnsupportedEncodingException e) {
			Log.e("CRAWLER", "performLogin", e);
			throw new CrawlerException();
		} catch (UnknownHostException e) {
			Log.e("CRAWLER", "performLogin", e);
			throw new CrawlerException("O serviço de consulta está indisponível no momento. Tente mais tarde");
		} catch (IOException e) {
			Log.e("CRAWLER", "performLogin", e);
			throw new CrawlerException();
		}
	}
	
	private void parseFgtsData(Document extratoDoc) {
		Element tableConta = extratoDoc.getElementsByTag("table").get(3);
		Elements tdsConta = tableConta.getElementsByTag("td");
		
		ContaFGTS contaFGTS = new ContaFGTS();
		this.contas.add(contaFGTS);
		contaFGTS.status= getTDValue(tdsConta.get(7)).trim();
		contaFGTS.empresa = getTDValue(tdsConta.get(2)); 
		contaFGTS.accountNumber = getTDValue(tdsConta.get(10)); 
		contaFGTS.saldo = getTDValue(tdsConta.get(16)).replaceFirst("</b>","");
		
		Element tableCreditos = extratoDoc.getElementsByTag("table").get(4);
		
		Elements lines = tableCreditos.getElementsByTag("tr");

		int indexCreditos = 3;
		if("I".equals(contaFGTS.status)){
			indexCreditos = 2;
		}

		for (int i = indexCreditos; i < lines.size(); i++) {
			CreditoFGTS credito = new CreditoFGTS();
			contaFGTS.creditos.add(credito);
			Elements line = lines.get(i).getElementsByTag("td");
			credito.date = line.get(0).html();
			credito.lancamento = line.get(1).html();
			credito.amount = "R$ "+line.get(2).html();
		}
	}

	private String getTDValue(Element element) {
		return element.html().split(":")[1];
	}

	public void consultaSeguro(String login, String senha) throws CrawlerException {
		try {
			HttpResponse performLoginResponse = performLogin(login, senha);
			Document document = validateLogin(performLoginResponse);
			ArrayList<BasicNameValuePair> params = loadEnvParams(document, "SDE");

			HttpPost processaSeguroPost = new HttpPost("https://webp.caixa.gov.br/cidadao/beneficios/SDE/sdepw011.asp");
			processaSeguroPost.addHeader(
					"User-Agent",
					"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");
			UrlEncodedFormEntity entityProcessaSeguro = new UrlEncodedFormEntity(params);
			processaSeguroPost.setEntity(entityProcessaSeguro);
			HttpResponse procSeguroResponse = this.httpClient.execute(processaSeguroPost, this.httpContext);

			if(procSeguroResponse.getStatusLine().getStatusCode() == 302 && "msgpw001.asp".equals(procSeguroResponse.getLastHeader("Location"))) {
				throw new CrawlerException("Não há dados para este PIS");
			}


			Document formSeguroDoc = Jsoup.parse(EntityUtils.toString(procSeguroResponse.getEntity()));
			Element formSeguro = formSeguroDoc.getElementsByTag("form").first();
            Elements hdnListaNumRequer = formSeguro.getElementsByAttributeValue("name", "hdnListaNumRequer");
            if(hdnListaNumRequer !=null && !hdnListaNumRequer.isEmpty() && !hdnListaNumRequer.val().trim().isEmpty()){
                for (String requerimento :  hdnListaNumRequer.val().split("\\|\\|")){
                    ArrayList<BasicNameValuePair> paramsRequerimento = new ArrayList<BasicNameValuePair>();
                    //paramsRequerimento.addAll(params);
                    String formattedRequerimento = NumberUtils.formatSeguroRequerimento(requerimento);
                    paramsRequerimento.add(new BasicNameValuePair("rdoRequer", formattedRequerimento));
                    paramsRequerimento.add(new BasicNameValuePair("hdnListaNumRequer", hdnListaNumRequer.val()));
                    paramsRequerimento.add(new BasicNameValuePair("hdnNomTrab", formSeguro.getElementsByAttributeValue("name", "hdnNomTrab").val()));
                    paramsRequerimento.add(new BasicNameValuePair("hdnNomMae", formSeguro.getElementsByAttributeValue("name", "hdnNomMae").val()));
                    paramsRequerimento.add(new BasicNameValuePair("hdnDtaNasc", formSeguro.getElementsByAttributeValue("name", "hdnDtaNasc").val()));


                    HttpPost processaSeguroRequerimentoPost = new HttpPost("https://webp.caixa.gov.br/cidadao/beneficios/SDE/sdepw002.asp");
                    processaSeguroRequerimentoPost.addHeader(
                            "User-Agent",
                            "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");
                    processaSeguroRequerimentoPost.addHeader(
                            "Referer",
                            "https://webp.caixa.gov.br/cidadao/beneficios/SDE/sdepw004.asp");

                    UrlEncodedFormEntity entityProcessaSeguroRequerimento = new UrlEncodedFormEntity(paramsRequerimento);
                    processaSeguroRequerimentoPost.setEntity(entityProcessaSeguroRequerimento);
                    HttpResponse procSeguroRequerimentoResponse = this.httpClient.execute(processaSeguroRequerimentoPost, this.httpContext);

                    String html = EntityUtils.toString(procSeguroRequerimentoResponse.getEntity());
                    if(procSeguroRequerimentoResponse.getStatusLine().getStatusCode() == 302 &&
                            "msgpw001.asp".equals(procSeguroRequerimentoResponse.getLastHeader("Location")) ||
                            html.contains("Mensagem")) {
                        Seguro seguro = new Seguro();

                        seguros.put(formattedRequerimento, seguro);
                    } else {

                        Document pageSeguro = Jsoup.parse(html);

                        Seguro seguro = getSeguroDataFromPage(pageSeguro);

                        seguros.put(seguro.nRequerimento, seguro);
                    }

                }


            } else {
                Seguro seguro = getSeguroData(formSeguro);
                seguros.put(seguro.nRequerimento, getSeguroData(formSeguro));
            }

		} catch (UnsupportedEncodingException e) {
			Log.e("CRAWLER", "performLogin", e);
			throw new CrawlerException();
		} catch (IOException e) {
			Log.e("CRAWLER", "performLogin", e);
			throw new CrawlerException();
		}

		
	}

    private Seguro getSeguroDataFromPage(Element pageSeguro) throws CrawlerException {
        Elements tableSeguro = pageSeguro.getElementsByTag("table").get(1).getElementsByTag("td");
        Elements tableParcela = pageSeguro.getElementsByTag("table").get(2).getElementsByTag("td");
        Seguro seguro = new Seguro();
        seguro.nome = tableSeguro.get(4).text();
        seguro.nRequerimento = tableSeguro.get(10).text();
        seguro.totalParcelas = tableSeguro.get(13).text();
        Integer qtdEvents  = Integer.valueOf(seguro.totalParcelas);
        int rowFirstElement = 4;
        for(int index = 0; index< qtdEvents; index++ ){
            seguro.eventos.add(
                    new Evento(tableParcela.get(rowFirstElement).text(),
                            "R$" + tableParcela.get(rowFirstElement +1).text(),
                            tableParcela.get(rowFirstElement + 2).text(),
                            tableParcela.get(rowFirstElement + 3).text())
            );
            rowFirstElement += 5;
        }
        return seguro;
    }

    @Deprecated
    private Seguro getSeguroData(Element formSeguro) throws CrawlerException {
        Seguro seguro = new Seguro();
        seguro.nome = formSeguro.getElementsByAttributeValue("name", "hdnNomTrab").val();
        seguro.nRequerimento = formSeguro.getElementsByAttributeValue("name", "hdnNumRequer").val();
        seguro.totalParcelas = formSeguro.getElementsByAttributeValue("name", "hdnQtdParcela").val();
        String qtdEventosValue = formSeguro.getElementsByAttributeValue("name", "hdnQtdEventos").val();

        if (seguro.nome == null || "".equals(seguro.nome)) {
            throw new CrawlerException("Não há dados para este PIS");
        }

        if (!"".equals(qtdEventosValue)) {
            String[] numParcelas = formSeguro.getElementsByAttributeValue("name", "hdnNumParcela").val().split("\\|\\|");
            String[] vlrParcela = formSeguro.getElementsByAttributeValue("name", "hdnVlrParcela").val().split("\\|\\|");
            String[] dtaInicio = formSeguro.getElementsByAttributeValue("name", "hdnDtaInicio").val().split("\\|\\|");
            String[] dtaFinal = formSeguro.getElementsByAttributeValue("name", "hdnDtaFinal").val().split("\\|\\|");
            String[] sitParcela = formSeguro.getElementsByAttributeValue("name", "hdnSitParcela").val().split("\\|\\|");
            int qtdEvents = Integer.valueOf(qtdEventosValue);
            for (int i = 0; i < qtdEvents; i++) {
                seguro.eventos.add(
                        new Evento(numParcelas[i],
                                "R$" + vlrParcela[i],
                                dtaInicio[i] + " à " + dtaFinal[i],
                                sitParcela[i])
                );
            }
        }
        return seguro;
    }

	private void seriralizeData(String type, Object data) {
		try {
			new Gson().toJson(data, new FileWriter(context.getFilesDir() + "/" + type));
			preferencesManager.saveUpdate(type);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void deseriralizeData(String type, Object data) {
		try {
			Class classToDeserialize;
			if("fgts".equals(type)){
				classToDeserialize = ArrayList.class;
			} else {
				classToDeserialize = HashMap.class;
			}
			new Gson().fromJson(new FileReader(context.getFilesDir() + "/" + type), classToDeserialize);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


}
