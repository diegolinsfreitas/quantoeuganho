package diego.freitas.quantoeuganho.fgts;

import java.io.Serializable;
import java.util.ArrayList;

public class Seguro implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String nome;
	public String nRequerimento;
	public String totalParcelas;
	
	public ArrayList<Evento> eventos = new ArrayList<Evento>();

	public boolean isValid() {
		return nRequerimento != null;
	}
	
}