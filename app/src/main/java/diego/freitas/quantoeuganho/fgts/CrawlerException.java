package diego.freitas.quantoeuganho.fgts;

/**
 * Created by diego on 14/02/16.
 */
public class CrawlerException  extends Exception{
    public static final String GENERAL_EXMESSAGE = "O site da caixa não está respondendo corretamente. Tente em outro momento.";

    public CrawlerException(){
        super(GENERAL_EXMESSAGE);
    }

    public CrawlerException(String message){
        super(message);
    }
}
