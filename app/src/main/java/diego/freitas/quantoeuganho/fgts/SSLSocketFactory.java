package diego.freitas.quantoeuganho.fgts;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.security.cert.Certificate;

public class SSLSocketFactory extends org.apache.http.conn.ssl.SSLSocketFactory {
	SSLContext sslContext = SSLContext.getInstance("TLS");
	
	private static final String PREFERRED_CIPHER_SUITE = "TLS_RSA_WITH_AES_128_CBC_SHA";

	public SSLSocketFactory(KeyStore paramKeyStore, InputStream certificatFile)
			throws NoSuchAlgorithmException, KeyManagementException,
			KeyStoreException, UnrecoverableKeyException, CertificateException, IOException {
		super(paramKeyStore);
		// Load CAs from an InputStream
		// (could be from a resource or ByteArrayInputStream or ...)
		CertificateFactory cf = CertificateFactory.getInstance("X.509");

		InputStream caInput = new BufferedInputStream(certificatFile);
		Certificate ca;
		try {
			ca = cf.generateCertificate(caInput);
		} finally {
			caInput.close();
		}

		// Create a KeyStore containing our trusted CAs
		String keyStoreType = KeyStore.getDefaultType();
		KeyStore keyStore = KeyStore.getInstance(keyStoreType);
		keyStore.load(null, null);
		keyStore.setCertificateEntry("ca", ca);

		// Create a TrustManager that trusts the CAs in our KeyStore
		String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
		tmf.init(keyStore);



		this.sslContext.init(null, tmf.getTrustManagers(), null);
	}
	
	public Socket createSocket() throws IOException {
		javax.net.ssl.SSLSocketFactory socketFactory = this.sslContext.getSocketFactory();
		Socket socket = socketFactory.createSocket();
		((SSLSocket)socket).setEnabledCipherSuites(((SSLSocket)socket).getSupportedCipherSuites());
		return socket;
	}

	public Socket createSocket(Socket paramSocket, String paramString,
			int paramInt, boolean paramBoolean) throws IOException,
			UnknownHostException {
		javax.net.ssl.SSLSocketFactory socketFactory = this.sslContext.getSocketFactory();
		Socket socket = socketFactory.createSocket(paramSocket,
				paramString, paramInt, paramBoolean);
		((SSLSocket)socket).setEnabledCipherSuites(((SSLSocket)socket).getSupportedCipherSuites());
		return socket;
	}

}
