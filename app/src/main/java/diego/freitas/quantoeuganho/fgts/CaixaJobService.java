package diego.freitas.quantoeuganho.fgts;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;
import java.util.HashMap;

import diego.freitas.quantoeuganho.ExtratoFGTSActivity;
import diego.freitas.quantoeuganho.PreferencesManager;
import diego.freitas.quantoeuganho.SeguroDesempregoActivity;
import diego.freitas.quantoeuganho.model.ContaFGTS;

/**
 * Created by diego.freitas on 23/12/2016.
 */
public class CaixaJobService extends JobService {


    private PreferencesManager preferenceManager;
    private String service;
    private DownloadStateReceiver mDownloadStateReceiver;
    private boolean reeschedule = true;

    private class DownloadStateReceiver extends BroadcastReceiver
    {
        // Prevents instantiation
        private DownloadStateReceiver() {
        }
        // Called when the BroadcastReceiver gets an Intent it's registered to receive
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("category").equals(DownloadDataService.ERROR_CATEGORY)){
                //LoginFGTSDialogFragment.this.error = intent.getStringExtra("error") +"\n Iremos avisar quando voltar ao normal pelo aplicativo";
                onPostExecute(null);
            } else if(intent.getStringExtra("category").equals(DownloadDataService.RESULT_CATEGORY)){
                onPostExecute(intent.getSerializableExtra("data"));
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        preferenceManager  = new PreferencesManager(getApplicationContext());
        IntentFilter statusIntentFilter = new IntentFilter(
                DownloadDataService.BROADCAST_ACTION);

        mDownloadStateReceiver =
                new DownloadStateReceiver();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mDownloadStateReceiver, statusIntentFilter);
        registerReceiver(mDownloadStateReceiver, statusIntentFilter);
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        service = params.getExtras().getString("service");
        Intent mServiceIntent = new Intent(this, DownloadDataService.class);
        mServiceIntent.putExtra("nis", preferenceManager.getNis());
        mServiceIntent.putExtra("password", preferenceManager.getPass());
        mServiceIntent.putExtra("service", service);
        startService(mServiceIntent);
        return false; // true if we're not done yet
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return reeschedule;
    }


    protected void onPostExecute(Object result) {

        if (result == null) {
            stopSelf();
            reeschedule = true;
        } else if ("fgts".equals(service)) {
            reeschedule = false;
            Intent intent = new Intent(this, ExtratoFGTSActivity.class);
            intent.putExtra("contas", (ArrayList<ContaFGTS>) result);
            startActivity(intent);

        } else if ("seguro".equals(service)) {
            reeschedule = false;
            Intent intent = new Intent(this, SeguroDesempregoActivity.class);
            intent.putExtra("seguro", (HashMap) result);
            startActivity(intent);
        }
    }


}