package diego.freitas.quantoeuganho.fgts;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.io.Serializable;

/**
 * Created by diego.freitas on 23/12/2016.
 */
public class DownloadDataService extends IntentService {

    public static final String BROADCAST_ACTION = "DownloadDataService";
    public static final String BROADCAST_ACTION_BACKGROUND = "DownloadDataService.BACKGRAOUND";
    public static final String ERROR_CATEGORY = "ERROR";
    public static final String RESULT_CATEGORY = "Result";
    private String service;


    public DownloadDataService() {
        super("DownloadDataService");
    }


    @Override
    protected void onHandleIntent(Intent workIntent) {
        // Gets data from the incoming Intent
        String nis = workIntent.getStringExtra("nis");
        String pass = workIntent.getStringExtra("password");
        service = workIntent.getStringExtra("service");


        try {
            Crawler crawler = new Crawler(DownloadDataService.this);
            if ("fgts".equals(service)) {
                crawler.carregarExtrado(nis, pass);
                broadcastResult(crawler.contas);
            } else if ("seguro".equals(service)) {
                crawler.consultaSeguro(nis, pass);
                broadcastResult(crawler.seguros);
            }
        } catch (CrawlerException e) {
            broadcastError(e.getMessage());
        } catch (Exception e) {
            broadcastError(e.getMessage());
        }
    }

    private void broadcastResult(Serializable data) {
        Intent intent = new Intent(BROADCAST_ACTION);
        intent.putExtra("category", RESULT_CATEGORY);
        intent.getExtras().putSerializable("data", data);
        intent.putExtra("service", service);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void broadcastError(String error) {
        Intent errorIntent = new Intent(BROADCAST_ACTION);
        errorIntent.putExtra("category", ERROR_CATEGORY);
        errorIntent.putExtra("error", error);
        errorIntent.putExtra("service", service);
        LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(errorIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
