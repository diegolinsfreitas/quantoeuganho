package diego.freitas.quantoeuganho;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import diego.freitas.quantoeuganho.util.FontUtils;

public class RemoveAdsDialogFragment extends DialogFragment {

	OnClickListener aggrededListener;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// this.setStyle(R.style.dialog_no_border, R.style.QuantoEuGanhoTheme);
		this.setCancelable(true);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.setCanceledOnTouchOutside(true);
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.remove_ads, container, false);
		FontUtils.overrideFonts(getActivity(), FontUtils.ROBOTO_LIGHT, v);
		v.findViewById(R.id.btn_agreed).setOnClickListener(
				aggrededListener);
		AdService adService = new AdService(getActivity());
		
		((TextView) v.findViewById(R.id.txv_instructions)).setText("Indique o aplicativo para "+ adService.minInvites() +" pessoas e as propagandas serão removidas");
		((TextView) v.findViewById(R.id.txv_missing_invites)).setText("Faltam "+(adService.minInvites() - adService.preferencesManager.invites())+" convites");
		return v;
	}

}
