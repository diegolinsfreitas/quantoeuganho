package diego.freitas.quantoeuganho;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class TutorialNISFragment extends DialogFragment{
	
	private TutorialListener listener;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	  Dialog dialog = super.onCreateDialog(savedInstanceState);
	  dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
	  dialog.setCanceledOnTouchOutside(false);
	  return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.tutorial_nis_number, container, false);
		TextView nisInstructions1 = (TextView) rootView.findViewById(R.id.txv_nis_instructions1);
		nisInstructions1.setMovementMethod(LinkMovementMethod.getInstance());
		
		View btnDimiss = rootView.findViewById(R.id.btn_dimiss);
		btnDimiss.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				TutorialListener listener = getListener();
				if (listener != null) {
					listener.close();
				}
			}
		});
		
		View btnOk = rootView.findViewById(R.id.btn_nis_ok);
		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				TutorialListener listener = getListener();
				if (listener != null) {
					listener.nisOk();
				}
			}
		});
        return rootView;
	}

	private TutorialListener getListener(){
		FragmentActivity activity = getActivity();
		if(activity != null && activity instanceof TutorialListener) {
			return (TutorialListener) activity;
		}
		return null;
	}

}
