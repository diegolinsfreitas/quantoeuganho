package diego.freitas.quantoeuganho;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import diego.freitas.quantoeuganho.model.ContaFGTS;
import diego.freitas.quantoeuganho.util.FontUtils;

public class AccountsyAdapter extends BaseAdapter {

	List<ContaFGTS> data;
	LayoutInflater layoutInflater;
	public Activity parentActivity;

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return data.get(position).id;
	}

	@Override
	public View getView(int index, View v, ViewGroup vGroup) {
		ViewHolder viewHolder;
		if (v == null) {
			v = layoutInflater.inflate(R.layout.account_item, vGroup, false);
			FontUtils.overrideFonts(layoutInflater.getContext(),
					FontUtils.ROBOTO_LIGHT, v);
			viewHolder = new ViewHolder();
			viewHolder.txvTitle = (TextView) v.findViewById(R.id.txv_account_title);
			viewHolder.txvValue = (TextView) v.findViewById(R.id.txv_amount);
			v.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) v.getTag();
		}

		final ContaFGTS fgts = (ContaFGTS) getItem(index);
		if (fgts != null) {
			loadView(viewHolder, fgts, index);
		}

		v.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(parentActivity, DetailsFGTSActivity.class);
				intent.putExtra("FGTS", fgts);
				parentActivity.startActivity(intent);
			}
		});
		return v;
	}

	private void loadView(ViewHolder viewHolder, final ContaFGTS fgts,
			final int index) {
		viewHolder.txvTitle.setText(fgts.empresa);
		viewHolder.txvValue.setText(fgts.saldo);
		viewHolder.itemId = fgts.accountNumber;
		viewHolder.conta = fgts;
	}

	static class ViewHolder {
		String itemId;
		ContaFGTS conta;
		TextView txvTitle;
		TextView txvValue;
	}

	@Override
	public boolean isEmpty() {
		return data.isEmpty();
	}

}
