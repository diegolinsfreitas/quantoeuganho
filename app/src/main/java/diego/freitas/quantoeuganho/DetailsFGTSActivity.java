package diego.freitas.quantoeuganho;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collections;

import diego.freitas.quantoeuganho.model.ContaFGTS;
import diego.freitas.quantoeuganho.util.FontUtils;

public class DetailsFGTSActivity extends FragmentActivity {

	private ListView lstDetails;
	private DetailsAccountAdapter detailsAccountAdapter;
	private AdService adService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details_account_fgts);
		adService = new AdService(this);
		adService.loadAd(this);
		lstDetails = (ListView) findViewById(R.id.lst_details);
		TextView emptyView = new TextView(this);

		emptyView.setText("Sem histórico de créditos...");

		
		emptyView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);

		android.widget.RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
				new LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT));
		layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,
				RelativeLayout.TRUE);
		emptyView.setLayoutParams(layoutParams);
		((ViewGroup) lstDetails.getParent()).addView(emptyView);
		lstDetails.setEmptyView(emptyView);
		detailsAccountAdapter = new DetailsAccountAdapter();
		detailsAccountAdapter.layoutInflater = getLayoutInflater();
		
		ContaFGTS contaFgts = (ContaFGTS) getIntent().getExtras().getSerializable("FGTS");
		Collections.reverse(contaFgts.creditos);
		detailsAccountAdapter.data = contaFgts.creditos;

		//AdMobUtil.configureAdMob(this, R.id.adv_history_activity,null);
		FontUtils.overrideFonts(this, FontUtils.ROBOTO_LIGHT, emptyView);
		FontUtils.overrideFonts(this, FontUtils.ROBOTO_LIGHT,
				findViewById(R.id.main_activity_container));
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		adService.onDestroy();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//adService.showInterstitialAd();
		adService.onPause();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		adService.showInterstitialAd();
	}

	
	@Override
	protected void onResume() {
		super.onResume();
		lstDetails.setAdapter(detailsAccountAdapter);
		adService.onResume();
	}

}
