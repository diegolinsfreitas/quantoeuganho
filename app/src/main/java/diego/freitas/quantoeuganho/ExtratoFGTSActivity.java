package diego.freitas.quantoeuganho;

import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.MultipleCategorySeries;
import org.achartengine.renderer.DialRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import diego.freitas.quantoeuganho.model.ContaFGTS;
import diego.freitas.quantoeuganho.util.FontUtils;
import diego.freitas.quantoeuganho.util.NumberUtils;
import im.delight.apprater.AppRater;

public class ExtratoFGTSActivity extends FragmentActivity implements
		OnClickListener {


	private static final int FIRST_ONRESUME = 1;
	private ViewGroup rlGraph;
	private View rlResults;
	private ScrollView hsv;
	private Handler handler = new Handler();

	private GraphicalView mChart;
	private LinearLayout llChart;
	
	private int[] chartColors= null;
	private AdService adService;
	private ListView listView;
	private int launchs = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_extrato_fgts);
		
		adService = new AdService(this);
		adService.loadAd(this);
		adService.loadAd(R.id.adView1);
		
		FontUtils.overrideFonts(this, FontUtils.ROBOTO_LIGHT,
				findViewById(R.id.scroll_container));

		rlGraph = (ViewGroup) findViewById(R.id.rl_graph_fgts);
		
		listView = (ListView) findViewById(R.id.ll_saldos);
		AccountsyAdapter adapter = new AccountsyAdapter();
		adapter.layoutInflater = getLayoutInflater();
		adapter.parentActivity = this;
		
		List<ContaFGTS> contas = (List<ContaFGTS>) getIntent().getExtras().getSerializable("contas");
		

		llChart = (LinearLayout) findViewById(R.id.ll_chart_fgts);
		TextView txtValorTotal = (TextView) findViewById(R.id.txv_saldo_value);
		

		chartColors = new int[contas.size()];
		
		int[] availableColors = new int[]{R.color.fgts1, R.color.fgts2,R.color.fgts3,R.color.fgts4,R.color.fgts5,R.color.fgts6,R.color.fgts7};
		for (int i = 0, tempIndex=0; i < chartColors.length; i++, tempIndex++) {
			
			if(tempIndex == availableColors.length){
				tempIndex = 0;
			}
			chartColors[i] = availableColors[tempIndex];
		}
		
		String[] labels = new String[contas.size()];
		double[] values = new double[contas.size()];
		int i = 0;
		double valorTotal = 0;
		for(ContaFGTS conta: contas){
			labels[i] = conta.empresa;
			values[i] = Double.valueOf(conta.saldo.replace("R$", "").replace(".", "").replace(",", ".").trim());
			valorTotal += values[i];
			i++;
		}
		txtValorTotal.setText(NumberUtils.formatCurrency(valorTotal));
		adapter.data = contas;
		launchs = contas.size()+FIRST_ONRESUME;
		listView.setAdapter(adapter);
		updateGraph(labels, values, valorTotal);

	}
	
	private void calculeHeightListView() {  
        int totalHeight = 0;  
  
        ListAdapter adapter = listView.getAdapter();  
        int lenght = adapter.getCount();  
  
        for (int i = 0; i < lenght; i++) {  
            View listItem = adapter.getView(i, null, listView);  
            listItem.measure(0, 0);  
            totalHeight += getResources().getDimension(R.dimen.height_list_item);  
        }  
  
        ViewGroup.LayoutParams params = listView.getLayoutParams();  
        params.height = totalHeight  
                + (listView.getDividerHeight() * (adapter.getCount() - 1));  
        listView.setLayoutParams(params);  
        listView.requestLayout();  
    }  
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		adService.onDestroy();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//adService.showInterstitialAd();
		adService.onPause();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		adService.onResume();
		calculeHeightListView();
		AppRater appRater = new AppRater(this);
		appRater.setLaunchesBeforePrompt(launchs);
		appRater.setDaysBeforePrompt(0);
		appRater.setPhrases(R.string.rate_title, R.string.rate_explanation,
				R.string.rate_now, R.string.rate_later, R.string.rate_never);
		appRater.show();
	}

	public void hideResultPanels() {
		rlGraph.setVisibility(View.GONE);
		rlResults.setVisibility(View.GONE);
	}

	public void showResultPanels() {
		rlGraph.setVisibility(View.VISIBLE);
		rlResults.setVisibility(View.VISIBLE);
		this.handler.post(new Runnable() {
			public void run() {
				hsv.smoothScrollBy(0, rlResults.getTop());
			}
		});

	}

	public void updateGraph(String[] labels, double[] data, double total) {
		// initChart();
		// addSampleData();
		MultipleCategorySeries category = new MultipleCategorySeries(
				"Distribuição Salário");
		

		final DialRenderer renderer = new DialRenderer();
		renderer.setZoomEnabled(false);
		renderer.setExternalZoomEnabled(false);
		renderer.setTextTypeface(FontUtils.getTypeFaceByName(this,
				FontUtils.ROBOTO_LIGHT));
		renderer.setLabelsTextSize(30);
		renderer.setLabelsColor(Color.BLACK);
		renderer.setShowLabels(true);
		renderer.setMinValue(0);
		renderer.setMaxValue(100);
		renderer.setPanEnabled(false);
		renderer.setShowLegend(false);
		SimpleSeriesRenderer r = null; 
		for (int i = 0; i < data.length; i++) {
			r = new SimpleSeriesRenderer();
			r.setColor(getResources().getColor(chartColors[i]));
			renderer.addSeriesRenderer(r);
			labels[i] = NumberUtils.formatPercent(data[i]/total) + " " +labels[i]; 
		}

		category.add(labels,data);
		
		if (mChart != null) {
			llChart.removeView(mChart);
		}
		mChart = ChartFactory.getDoughnutChartView(this, category, renderer);
		//mChart.setHoleRadius();
		mChart.setBackgroundColor(Color.WHITE);
		llChart.addView(mChart);

	}

	@Override
	public void onClick(View v) {
		
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		adService.showInterstitialAd();

	}
}
