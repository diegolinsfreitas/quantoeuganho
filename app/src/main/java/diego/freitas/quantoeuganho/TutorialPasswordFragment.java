package diego.freitas.quantoeuganho;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;

public class TutorialPasswordFragment extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.tutorial_nis_password, container, false);
        View btnDimiss = rootView.findViewById(R.id.btn_dimiss);
        btnDimiss.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                TutorialListener listener = getListener();
                if (listener != null) {
                    listener.close();
                }
            }
        });

        View btnOk = rootView.findViewById(R.id.btn_pass_ok);
        btnOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                TutorialListener listener = getListener();
                if (listener != null) {
                    listener.passwordOk();
                }


            }
        });
        return rootView;
    }

    private TutorialListener getListener() {
        FragmentActivity activity = getActivity();
        if (activity != null && activity instanceof TutorialListener) {
            return (TutorialListener) activity;
        }
        return null;
    }

}
