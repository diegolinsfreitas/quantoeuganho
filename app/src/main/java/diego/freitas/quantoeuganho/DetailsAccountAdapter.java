package diego.freitas.quantoeuganho;

import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import diego.freitas.quantoeuganho.model.CreditoFGTS;
import diego.freitas.quantoeuganho.util.FontUtils;
import diego.freitas.quantoeuganho.util.NumberUtils;

public class DetailsAccountAdapter extends BaseAdapter {

	List<CreditoFGTS> data;
	LayoutInflater layoutInflater;

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return data.get(position).hashCode();
	}

	@Override
	public View getView(int index, View v, ViewGroup vGroup) {
		ViewHolder viewHolder;
		if (v == null) {
			v = layoutInflater.inflate(R.layout.details_account_fgts_item, vGroup, false);
			FontUtils.overrideFonts(layoutInflater.getContext(),
					FontUtils.ROBOTO_LIGHT, v);
			viewHolder = new ViewHolder();
			viewHolder.txvTitle = (TextView) v.findViewById(R.id.txv_title);
			viewHolder.txvValue = (TextView) v.findViewById(R.id.txv_amount);
			viewHolder.txvDate = (TextView) v.findViewById(R.id.txv_date);
			v.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) v.getTag();
		}

		final CreditoFGTS fgts = (CreditoFGTS) getItem(index);
		if (fgts != null) {
			loadView(viewHolder, fgts);
		}

		v.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
			}
		});
		return v;
	}

	private void loadView(ViewHolder viewHolder, final CreditoFGTS fgts) {
		viewHolder.txvTitle.setText(fgts.lancamento);
		viewHolder.txvValue.setText(fgts.amount);
		viewHolder.txvDate.setText(fgts.date);
	}

	static class ViewHolder {
		String itemId;
		TextView txvTitle;
		TextView txvValue;
		TextView txvDate;
	}

	@Override
	public boolean isEmpty() {
		return data.isEmpty();
	}

}
