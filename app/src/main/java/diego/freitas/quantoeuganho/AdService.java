package diego.freitas.quantoeuganho;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class AdService extends AdListener {
	
	

	public static boolean ADS_ENABLE = true;

	private static final int TOTAL_INTERACTION_SHOW_FULLAD = 3;

	private static final String TEST_DEVICE = "78CB2C84D47C251A0949AA4F74B0B2F9";

	public SharedPreferences prefs;

	private Activity ctx;

	private static InterstitialAd interstitialAd;

	private static final String AD_UNIT_ID_INTERSTITIAL = "ca-app-pub-0555397782109166/6720213195";

	private AdView adView;

	public PreferencesManager preferencesManager;

	private String readFile(String pathname) throws IOException {

		File file = new File(pathname);
		StringBuilder fileContents = new StringBuilder((int)file.length());
		Scanner scanner = new Scanner(file);
		String lineSeparator = System.getProperty("line.separator");

		try {
			while(scanner.hasNextLine()) {
				fileContents.append(scanner.nextLine() + lineSeparator);
			}
			return fileContents.toString();
		} finally {
			scanner.close();
		}
	}
	public AdService(Activity ctx) {
		this.ctx = ctx;




		preferencesManager = new PreferencesManager(ctx);

		ADS_ENABLE = preferencesManager.ads();
		if(ADS_ENABLE && interstitialAd == null){
			interstitialAd = new InterstitialAd(ctx.getApplicationContext());
			interstitialAd.setAdUnitId(AD_UNIT_ID_INTERSTITIAL);
		}
		if(ADS_ENABLE && !interstitialAd.isLoaded() && !interstitialAd.isLoading()){
			loadInterstitialAd();
		}
		prefs = preferencesManager.preferences;
	}
	
	public int minInvites() {
		int[] groups = new int[]{3, 5, 8};
		if(prefs.getAll().containsKey("min_invites")){
			return prefs.getInt("min_invites",5);
		} else {
			Random random = new Random();
			int indexGroup = random.nextInt( groups.length );
			int minInvites = prefs.getInt("min_invites", groups[indexGroup]);
			Editor edit = prefs.edit();
			edit.putInt("min_invites", minInvites);
			edit.putInt("invite_group", indexGroup);
			edit.commit();
			QuantoEuGanhoApplication.gaTracker.group(indexGroup);
			return minInvites;
		}
	}
	
	public int inviteGroup(){
		return prefs.getInt("invite_group", 0);
	}

	private void loadInterstitialAd() {
		if (ADS_ENABLE) {
			AdRequest interstitialRequest = buildAdRequest();
			interstitialAd.setAdListener(new AdListener() {
				@Override
				public void onAdLoaded() {
					
				}

				@Override
				public void onAdClosed() {
					
				}
			});
			interstitialAd.loadAd(interstitialRequest);
		}
		
	}

	private AdRequest buildAdRequest() {
		AdRequest.Builder builder = new AdRequest.Builder();
		builder.addTestDevice(TEST_DEVICE);
		AdRequest interstitialRequest = builder.build();
		return interstitialRequest;
	}

	public void showInterstitialAd() {
		if (ADS_ENABLE) {
			int openInterAdTrigger = getInteractionCount();
			openInterAdTrigger++;
			if (openInterAdTrigger >= TOTAL_INTERACTION_SHOW_FULLAD) {
				showOrLoadInterstital();
				openInterAdTrigger = 0;
			}
			prefs.edit().putInt("count", openInterAdTrigger).commit();
		}
		
	}

	private int getInteractionCount() {
		return prefs.getInt("count", 0);
	}

	private void showOrLoadInterstital() {
		if (ADS_ENABLE) {
			try {
				ctx.runOnUiThread(new Runnable() {

					public void run() {
						if(interstitialAd.isLoaded()){
							interstitialAd.show();
						}
						if(!interstitialAd.isLoading()){
							loadInterstitialAd();
						}
					}
				});

			} catch (Exception e) {
				Log.e(MainActivity.class.getName(), e.getMessage());
			}
		}
	}

	public void onDestroy() {
		this.ctx = null;
	}

	public void loadAd(Activity activity) {
		loadAd(R.id.adView);
	}

	public void loadAd(int addView) {
		adView = (AdView) this.ctx.findViewById(addView);
		if (ADS_ENABLE) {
			AdRequest adRequest = new AdRequest.Builder().addTestDevice(
					TEST_DEVICE).build();
			adView.setAdListener(this);
			adView.loadAd(adRequest);
		} else {
			adView.setVisibility(View.GONE);
		}
	}

	public void onPause() {
		adView.pause();
	}

	public void onResume() {
		adView.resume();
	}

}
