package diego.freitas.quantoeuganho;


import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import diego.freitas.quantoeuganho.util.FontUtils;

public class AboutDialogFragment extends DialogFragment {
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	  Dialog dialog = super.onCreateDialog(savedInstanceState);
	  dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
	  dialog.setCanceledOnTouchOutside(false);
	  return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.about, container, false);
		FontUtils.overrideFonts(getActivity(), FontUtils.ROBOTO_LIGHT, v);
		View tv = v.findViewById(R.id.txv_version);
		try {
			((TextView) tv).setText("Versão "+getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName);
		} catch (NameNotFoundException e) {
			((TextView) tv).setText("Versão 1.0");
		}

		// Watch for button clicks.
		v.findViewById(R.id.about_btn_dismiss).setOnClickListener(
				new OnClickListener() {
					public void onClick(View v) {
						AboutDialogFragment.this.dismiss();
					}
				});
		v.findViewById(R.id.btn_site).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://br.linkedin.com/in/diegolins"));
				startActivity(browserIntent);
				
			}
		});

		return v;
	}

}
