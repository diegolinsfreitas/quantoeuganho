package diego.freitas.quantoeuganho;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.MultipleCategorySeries;
import org.achartengine.renderer.DialRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;
import diego.freitas.quantoeuganho.model.History;
import diego.freitas.quantoeuganho.model.SalaryLiquidDataParser;
import diego.freitas.quantoeuganho.presenter.SalarioLiquidoPresenter;
import diego.freitas.quantoeuganho.util.FontUtils;
import diego.freitas.quantoeuganho.util.NumberTextWatcher;
import diego.freitas.quantoeuganho.util.NumberUtils;
import diego.freitas.quantoeuganho.util.Validation;

public class SalarioLiquidoActivity extends FragmentActivity implements
		OnClickListener {

	private View imvInssTip;
	private View txvInss;
	private View btnCalc;
	private EditText edtSalary;
	private EditText edtBenefits;
	private EditText edtDepenents;
	private EditText edtOtherDiscounts;
	private SalarioLiquidoPresenter salarioLiquidoPresenter;
	private TextView txvSalBrutoValue;
	private TextView txvBenefitsValue;
	private TextView txvInssValue;
	private TextView txvIrrfValue;
	private TextView txvOthersValue;
	private TextView txvProventosValue;
	private TextView txvDescontos;
	private TextView txvSalResult;
	private PopupWindow popupInssTip;
	private PopupWindow popupIrrfTip;
	private View imvIrrfTip;
	private View txvIrrf;
	private View btnClear;
	private ViewGroup rlGraph;
	private View rlResults;
	private ScrollView hsv;
	private Handler handler = new Handler();
	private diego.freitas.quantoeuganho.MenuDelegate menuDelegate;

	private GraphicalView mChart;
	private LinearLayout llChart;
	
	private int[] CHART_SERIES_COLORS= new int[]{R.color.chart_serie1,R.color.chart_serie2,R.color.chart_serie3,R.color.chart_serie4,R.color.chart_serie5};
	private AdService adService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_salario_liquido);
		salarioLiquidoPresenter = new SalarioLiquidoPresenter(this);
		adService = new AdService(this);
		adService.loadAd(this);
		// FontUtils.overrideFonts(this, FontUtils.ROBOTO_LIGHT,
		// actionBar.getCustomView());
		FontUtils.overrideFonts(this, FontUtils.ROBOTO_LIGHT,
				findViewById(R.id.scroll_container));

		imvInssTip = findViewById(R.id.imv_tip_inss);
		txvInss = findViewById(R.id.txv_inss);
		txvInss.setOnClickListener(this);
		imvInssTip.setOnClickListener(this);

		imvIrrfTip = findViewById(R.id.imv_tip_irrf);
		txvIrrf = findViewById(R.id.txv_irrf);
		txvIrrf.setOnClickListener(this);
		imvIrrfTip.setOnClickListener(this);

		edtSalary = (EditText) findViewById(R.id.edt_brute_salary);
		edtBenefits = (EditText) findViewById(R.id.edt_benefits);
		edtDepenents = (EditText) findViewById(R.id.edt_n_dependents);
		edtOtherDiscounts = (EditText) findViewById(R.id.edt_others_discount);
		btnCalc = findViewById(R.id.btn_calc);
		btnCalc.setOnClickListener(this);
		btnClear = findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(this);

		rlGraph = (ViewGroup) findViewById(R.id.rl_graph);
		rlResults = findViewById(R.id.rl_results);

		hideResultPanels();
		NumberTextWatcher.applyCurrency(edtSalary);
		NumberTextWatcher.applyCurrency(edtOtherDiscounts);
		NumberTextWatcher.applyPercent(edtBenefits);
		NumberTextWatcher.applyInteger(edtDepenents);

		Validation.applyRequiredValidation(edtSalary);

		popupInssTip = buildPopup(R.layout.popup_tip_content);
		popupIrrfTip = buildPopup(R.layout.popup_tip_content);

		hsv = (ScrollView) findViewById(R.id.scroll_container);
		//AdMobUtil.configureAdMob(this, R.id.adv_salary_activity, null);
		menuDelegate = new MenuDelegate(this);
		llChart = (LinearLayout) findViewById(R.id.ll_chart);

	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		adService.onDestroy();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//adService.showInterstitialAd();
		adService.onPause();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		adService.showInterstitialAd();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		History history = (History) getIntent().getSerializableExtra("history");
		String historyData = (String) getIntent()
				.getStringExtra("history_data");
		if (history != null) {
			SalaryLiquidDataParser parser = new SalaryLiquidDataParser(
					historyData);
			edtSalary.setText(String.valueOf(history.amount * 10));
			if (parser.getBenefit() != null) {
				edtBenefits.setText(String.valueOf(parser.getBenefit()*10));
			}

			if (parser.getDependents() != null) {
				edtDepenents.setText(String.valueOf(parser.getDependents()));
			}

			if (parser.getDiscounts() != null) {
				edtOtherDiscounts
						.setText(String.valueOf(parser.getDiscounts() * 10));
			}

			this.salarioLiquidoPresenter
					.performBtnCalcClick(
							history.amount,
							parser.getBenefit() == null ? 0 : parser
									.getBenefit(),
							parser.getDependents() == null ? 0 : parser
									.getDependents(),
							parser.getDiscounts() == null ? 0d : parser
									.getDiscounts(), false);
		}
		adService.onResume();
	}

	public void hideResultPanels() {
		rlGraph.setVisibility(View.GONE);
		rlResults.setVisibility(View.GONE);
	}

	public void showResultPanels() {
		rlGraph.setVisibility(View.VISIBLE);
		rlResults.setVisibility(View.VISIBLE);
		this.handler.post(new Runnable() {
			public void run() {
				hsv.smoothScrollBy(0, rlResults.getTop());
			}
		});

	}

	public void updateGraph(String[] labels, double[] data) {
		// initChart();
		// addSampleData();
		MultipleCategorySeries category = new MultipleCategorySeries(
				"Distribuição Salário");
		

		final DialRenderer renderer = new DialRenderer();
		renderer.setZoomEnabled(false);
		renderer.setExternalZoomEnabled(false);
		renderer.setTextTypeface(FontUtils.getTypeFaceByName(this,
				FontUtils.ROBOTO_LIGHT));
		renderer.setLabelsTextSize(30);
		renderer.setLabelsColor(Color.BLACK);
		renderer.setShowLabels(true);
		renderer.setMinValue(0);
		renderer.setMaxValue(100);
		renderer.setPanEnabled(false);
		renderer.setShowLegend(false);
		SimpleSeriesRenderer r = null; 
		for (int i = 0; i < data.length; i++) {
			r = new SimpleSeriesRenderer();
			r.setColor(getResources().getColor(CHART_SERIES_COLORS[i]));
			renderer.addSeriesRenderer(r);
			labels[i] = labels[i]+" " + NumberUtils.formatPercent(data[i]/100); 
		}

		category.add(labels,data);
		
		if (mChart != null) {
			llChart.removeView(mChart);
		}
		mChart = ChartFactory.getDoughnutChartView(this, category, renderer);
		mChart.setBackgroundColor(Color.WHITE);
		llChart.addView(mChart);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return menuDelegate.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return menuDelegate.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imv_tip_inss:
			showInssTip();
			break;
		case R.id.txv_inss:
			showInssTip();
			break;
		case R.id.imv_tip_irrf:
			showIrrfTip();
			break;
		case R.id.txv_irrf:
			showIrrfTip();
			break;
		case R.id.btn_calc:
			onBtnCalcClick();
			break;
		case R.id.btn_clear:
			onBtnClearClick();
		default:
			break;
		}
	}

	private void onBtnClearClick() {

		salarioLiquidoPresenter.performBtnClearClick();
	}

	public void clearFields() {
		edtBenefits.setText("");
		edtDepenents.setText("");
		edtOtherDiscounts.setText("");
		edtSalary.setText("");
		edtSalary.requestFocus();
	}

	private void onBtnCalcClick() {
		int dependents = 0;
		Double salary = 0d;
		Double benefits = 0d;
		Double others = 0d;
		if (checkValidation()) {
			salary = NumberUtils.parseCurrency(edtSalary.getText().toString());
			benefits = NumberUtils.parseNumber(
					edtBenefits.getText().toString()).doubleValue();
			dependents = NumberUtils.parseInteger(edtDepenents.getText()
					.toString());
			others = NumberUtils.parseCurrency(edtOtherDiscounts.getText()
					.toString());
			this.salarioLiquidoPresenter.performBtnCalcClick(salary, benefits,
					dependents, others, true);

		}
	}

	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.hasText(edtSalary)
				|| NumberUtils.parseCurrency(edtSalary.getText().toString()) <= 0) {
			edtSalary.setError(Validation.REQUIRED_MSG);
			ret = false;
		}

		return ret;
	}

	private void showInssTip() {
		popupInssTip.showAsDropDown(imvInssTip);
	}

	private void showIrrfTip() {
		popupIrrfTip.showAsDropDown(imvIrrfTip);
	}

	private PopupWindow buildPopup(int content) {
		View popupView = getLayoutInflater().inflate(content, null);

		PopupWindow popupTip = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		popupTip.setFocusable(true);
		popupTip.setOutsideTouchable(true);
		popupTip.setBackgroundDrawable(new BitmapDrawable());
		return popupTip;
	}

	public void setSalaryResult(double doubleValue) {
		txvSalBrutoValue = (TextView) findViewById(R.id.txv_sal_bruto_value);
		txvSalBrutoValue.setText(NumberUtils.formatCurrency(doubleValue));
	}

	public void setBenefitResult(double doubleValue) {
		txvBenefitsValue = (TextView) findViewById(R.id.txv_benefits_value);
		txvBenefitsValue.setText(NumberUtils.formatCurrency(doubleValue));
	}

	public void setInssResult(double doubleValue) {
		txvInssValue = (TextView) findViewById(R.id.txv_inss_value);
		txvInssValue.setText(NumberUtils.formatCurrency(doubleValue));
	}

	public void setIrrfResult(double doubleValue) {
		txvIrrfValue = (TextView) findViewById(R.id.txv_irrf_value);
		txvIrrfValue.setText(NumberUtils.formatCurrency(doubleValue));
	}

	public void setOtherDiscount(double doubleValue) {
		txvOthersValue = (TextView) findViewById(R.id.txv_others_value);
		txvOthersValue.setText(NumberUtils.formatCurrency(doubleValue));
	}

	public void setProventos(double doubleValue) {
		txvProventosValue = (TextView) findViewById(R.id.txv_proventos);
		txvProventosValue.setText(NumberUtils.formatCurrency(doubleValue));
	}

	public void setDicounts(double doubleValue) {
		txvDescontos = (TextView) findViewById(R.id.txv_descontos);
		txvDescontos.setText(NumberUtils.formatCurrency(doubleValue));
	}

	public void setLiquid(double doubleValue) {
		txvSalResult = (TextView) findViewById(R.id.txv_sal_result);
		txvSalResult.setText(NumberUtils.formatCurrency(doubleValue));
	}

	public void setIrrfRate(String rate) {

		((TextView) popupIrrfTip.getContentView()
				.findViewById(R.id.txv_message)).setText(rate);
	}

	public void setInssRate(String rate) {
		((TextView) popupInssTip.getContentView()
				.findViewById(R.id.txv_message)).setText(rate);

	}
}
