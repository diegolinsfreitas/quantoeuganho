package diego.freitas.quantoeuganho.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import diego.freitas.quantoeuganho.QuantoEuGanhoApplication;
import diego.freitas.quantoeuganho.model.CalcType;
import diego.freitas.quantoeuganho.model.History;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class HistoryDAO {
	
	public List<History> getHistory(){
		SQLiteDatabase writableDatabase = QuantoEuGanhoApplication.qegDBHelper.getDatabase();
		Cursor cursor = writableDatabase.rawQuery("select * from history", null);
		ArrayList<History> result = new ArrayList<History>(cursor.getCount());
		while (cursor.moveToNext()) { //
			History history = convertToModel(cursor);
			result.add(history);
		}
		cursor.close();
		return result;
	}
	
	public void saveHistory(History h){
		SQLiteDatabase database = QuantoEuGanhoApplication.qegDBHelper.getDatabase();
		try {
			database.beginTransaction();	
        	database.insert(HistoryMetadata.TABLE_NAME, null, toContentValues(h));
        	database.setTransactionSuccessful();
        } finally {
        	database.endTransaction();
        }
		
	}
	
	private ContentValues toContentValues(History h) {
		ContentValues values = new ContentValues();
		values.put(HistoryMetadata.C_CREATED, new Date().getTime());
		values.put(HistoryMetadata.C_TYPE, h.type.ordinal());
		values.put(HistoryMetadata.C_DATA, h.data.toString());
		values.put(HistoryMetadata.C_AMOUNT, h.amount);
		return values;
	}

	private History convertToModel(Cursor cursor) {
		History history = new History();
		history.id = cursor.getInt(cursor.getColumnIndex(HistoryMetadata.C_ID));
		history.type = CalcType.values()[cursor.getInt(cursor.getColumnIndex(HistoryMetadata.C_TYPE))];
		history.created = new Date();
		history.created.setTime(cursor.getLong((cursor.getColumnIndex(HistoryMetadata.C_CREATED))));
		history.amount = cursor.getDouble(cursor.getColumnIndex(HistoryMetadata.C_AMOUNT));
		try {
			history.data = new JSONObject(cursor.getString(cursor.getColumnIndex(HistoryMetadata.C_DATA)));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return history;
	}

	public void delete(int itemId) {
		SQLiteDatabase database = QuantoEuGanhoApplication.qegDBHelper.getDatabase();
		try {
			database.beginTransaction();	
        	database.delete(HistoryMetadata.TABLE_NAME, HistoryMetadata.C_ID+ "=?", new String[]{String.valueOf(itemId)});
        	database.setTransactionSuccessful();
        } finally {
        	database.endTransaction();
        }
	}

}
