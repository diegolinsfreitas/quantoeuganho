package diego.freitas.quantoeuganho.persistence;

import android.provider.BaseColumns;

public class HistoryMetadata {
	
	public static final String TABLE_NAME = "history";
	public static final String C_ID = BaseColumns._ID;
	public static final String C_DATA = "data";
	public static final String C_TYPE = "type";
	public static final String C_CREATED = "created_at";
	public static final String C_AMOUNT = "amount";
	

}
