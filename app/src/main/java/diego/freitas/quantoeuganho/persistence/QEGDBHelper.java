package diego.freitas.quantoeuganho.persistence;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class QEGDBHelper extends SQLiteOpenHelper{
	
	static final String TAG = "DbHelper";
	static final String DB_NAME = "cltpocket.db"; //
	static final int DB_VERSION = 2; //
	private SQLiteDatabase database;
	
	

	public QEGDBHelper(Application app) {
		super(app, DB_NAME, null, DB_VERSION);
		database = this.getWritableDatabase();
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("drop table if exists history");
		ddlv2(db);
	}

	private void ddlv2(SQLiteDatabase db) {
		StringBuffer sql = new StringBuffer();
		sql
		.append("create table ")
			.append( HistoryMetadata.TABLE_NAME )
					.append(" (")
							.append( HistoryMetadata.C_ID + " integer primary key AUTOINCREMENT, ")
							.append( HistoryMetadata.C_TYPE + " int, " )
							.append( HistoryMetadata.C_DATA + " text, ") 
							.append( HistoryMetadata.C_CREATED + " int, ") 
							.append( HistoryMetadata.C_AMOUNT + " real ") 
					.append(" );");
		db.execSQL(sql.toString());
	}
	
	

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//ddlv2(db);
	}

	public SQLiteDatabase getDatabase() {
		return database;
	}


}
