package diego.freitas.quantoeuganho;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import diego.freitas.quantoeuganho.fgts.Seguro;
import diego.freitas.quantoeuganho.util.FontUtils;

public class SeguroDesempregoActivity extends FragmentActivity implements
		OnClickListener {

	private AdService adService;
	private ListView listView;
	private HashMap seguros;
	private EventosAdapter adapter;
	private List<String> spinnerArray;
	private View content;
	private View header;
	private View noDataView;
	private View events;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_seguro);
		
		adService = new AdService(this);
		adService.loadAd(this);
		adService.loadAd(R.id.adView);
		
		FontUtils.overrideFonts(this, FontUtils.ROBOTO_LIGHT,
				findViewById(R.id.scroll_container));

		content = findViewById(R.id.content);
		events = findViewById(R.id.scroll_events);
		header = findViewById(R.id.eventos);
		
		listView = (ListView) findViewById(R.id.ll_events);
		adapter = new EventosAdapter();
		adapter.layoutInflater = getLayoutInflater();
		adapter.parentActivity = this;

		seguros = (HashMap) getIntent().getExtras().getSerializable("seguro");
		noDataView = findViewById(R.id.no_data);

		spinnerArray = new ArrayList<String>();
		spinnerArray.addAll(seguros.keySet());

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, spinnerArray);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Spinner sItems = (Spinner) findViewById(R.id.spinner);
		sItems.setAdapter(adapter);

		sItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				loadData((Seguro) seguros.get(spinnerArray.get(position)));
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});





	}

	private void loadData(Seguro seguro) {
		
		if(seguro.isValid()){
			content.setVisibility(View.VISIBLE);
			header.setVisibility(View.VISIBLE);
			events.setVisibility(View.VISIBLE);
			noDataView.setVisibility(View.GONE);

			TextView txtNome = (TextView) findViewById(R.id.txt_nome_value);
			txtNome.setText(seguro.nome);
			TextView txtReq = (TextView) findViewById(R.id.txt_req_value);
			txtReq.setText(seguro.nRequerimento);
			TextView txtParcelas = (TextView) findViewById(R.id.txt_qtd_value);
			txtParcelas.setText(seguro.totalParcelas);

			adapter.data = seguro.eventos;


			listView.setAdapter(adapter);
			listView.invalidate();
			calculeHeightListView();
		} else {
			content.setVisibility(View.GONE);
			header.setVisibility(View.GONE);
			noDataView.setVisibility(View.VISIBLE);
			events.setVisibility(View.GONE);
		}
		
	}

	private void calculeHeightListView() {  
        int totalHeight = 0;  
  
        ListAdapter adapter = listView.getAdapter();  
        int lenght = adapter.getCount();  
  
        for (int i = 0; i < lenght; i++) {  
            View listItem = adapter.getView(i, null, listView);  
            listItem.measure(0, 0);  
            totalHeight += getResources().getDimension(R.dimen.height_list_item);  
        }  
  
        ViewGroup.LayoutParams params = listView.getLayoutParams();  
        params.height = totalHeight  
                + (listView.getDividerHeight() * (adapter.getCount() - 1));  
        listView.setLayoutParams(params);  
        listView.requestLayout();
    }  
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		adService.onDestroy();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//adService.showInterstitialAd();
		adService.onPause();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		adService.onResume();
	}

	@Override
	public void onClick(View v) {
		
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		adService.showInterstitialAd();
	}
}
