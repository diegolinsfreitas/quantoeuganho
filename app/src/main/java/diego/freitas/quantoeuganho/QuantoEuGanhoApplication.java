package diego.freitas.quantoeuganho;

import android.app.Application;
import diego.freitas.quantoeuganho.persistence.QEGDBHelper;

public class QuantoEuGanhoApplication extends Application{
	
	public static QEGDBHelper qegDBHelper;
	public static GATracker gaTracker;

	public static QuantoEuGanhoApplication instance = null;


	@Override
	public void onCreate() {
		super.onCreate();
		qegDBHelper = new QEGDBHelper(this);
		gaTracker = new GATracker(this);
		instance = this;
	}
}
