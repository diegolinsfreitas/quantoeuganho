package diego.freitas.quantoeuganho;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.Calendar;

public class PreferencesManager {

	private static final String INVITES = "Invites";
	private static final String ADS = "ads";
	private static final String PASSWORD = "password";
	private static final String NIS = "nis";
	public static final String PREF_MANANER_KEY = "diego.freitas.quantoeuganho.E";

	public PreferencesManager(Context tutorialNisActivity) {
		preferences = tutorialNisActivity.getSharedPreferences(PREF_MANANER_KEY, Activity.MODE_PRIVATE);
	}
	public static final String DISCLAIMER_ACCEPTED = "disclaimer_accepted";
	public static final String TUTORIAL_UNDERTOOD = "tutorial_undertood";
	SharedPreferences preferences;
	
	public void tutorialUnderstood() {
		Editor editor = preferences.edit();
		editor.putBoolean(TUTORIAL_UNDERTOOD, true);
		editor.commit();
		
	}
	public boolean understoodTutorial() {
		return preferences.getBoolean(TUTORIAL_UNDERTOOD, false);
	}
	public CharSequence getNis() {
		return preferences.getString(NIS, "");
	}
	public CharSequence getPass() {
		return preferences.getString(PASSWORD,  "");
	}
	
	public void saveLogin(String nis, String pass) {
		Editor editor = preferences.edit();
		editor.putString(NIS, nis);
		editor.putString(PASSWORD, pass);
		editor.commit();
	}
	
	public void removeAds() {
		Editor editor = preferences.edit();
		editor.putBoolean(ADS, false);
		editor.commit();
	}
	
	public boolean ads() {
		return preferences.getBoolean(ADS, true);
	}
	public int invites() {
		return preferences.getInt(INVITES, 0);
	}
	
	public void invites(int newInvites) {
		Editor edit = preferences.edit();
		edit.putInt(INVITES, invites() + newInvites);
		edit.commit();
	}

	public void saveUpdate(String type) {
		Editor edit = preferences.edit();
		edit.putLong("update_" + type, Calendar.getInstance().getTimeInMillis());
		edit.commit();
	}

	public Calendar getUpdate(String type) {
		Calendar instance = Calendar.getInstance();
		instance.setTimeInMillis(preferences.getLong("update_" + type, 0));
		return instance;
	}
}
