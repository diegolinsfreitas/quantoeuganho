package diego.freitas.quantoeuganho;

import diego.freitas.quantoeuganho.model.History;

public interface OpenCalcAction {

	void open(History history);

}
