package diego.freitas.quantoeuganho;

import java.math.BigDecimal;

import diego.freitas.quantoeuganho.R;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;
import diego.freitas.quantoeuganho.model.History;
import diego.freitas.quantoeuganho.model.SaldoFgtsDataParser;
import diego.freitas.quantoeuganho.presenter.SaldoFgtsPresenter;
import diego.freitas.quantoeuganho.util.FontUtils;
import diego.freitas.quantoeuganho.util.NumberTextWatcher;
import diego.freitas.quantoeuganho.util.NumberUtils;
import diego.freitas.quantoeuganho.util.Validation;

public class SaldoFgtsActivity extends FragmentActivity implements
		OnClickListener {

	private View imvFgtsTip;
	private View txvFgts;
	private View btnCalc;
	private EditText edtSalary;
	private EditText edtMeses;
	private SaldoFgtsPresenter saldoFgtsPresenter;
	private TextView txvFgtsValue;
	private TextView txvMeses;
	private TextView txvFgtsResult;
	private PopupWindow popupFgtsTip;
	private View btnClear;
	private View rlResults;
	private ScrollView hsv;
	private Handler handler = new Handler();
	private diego.freitas.quantoeuganho.MenuDelegate menuDelegate;
	private AdService adService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fgts);
		saldoFgtsPresenter = new SaldoFgtsPresenter(this);
		adService = new AdService(this);
		adService.loadAd(this);
		FontUtils.overrideFonts(this, FontUtils.ROBOTO_LIGHT,
				findViewById(R.id.scroll_container));

		imvFgtsTip = findViewById(R.id.imv_tip_fgts);
		txvFgts = findViewById(R.id.txv_fgts);
		imvFgtsTip.setOnClickListener(this);
		txvFgts.setOnClickListener(this);

		edtSalary = (EditText) findViewById(R.id.edt_brute_salary);
		edtMeses = (EditText) findViewById(R.id.edt_n_meses);

		btnCalc = findViewById(R.id.btn_calc);
		btnCalc.setOnClickListener(this);
		btnClear = findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(this);

		rlResults = findViewById(R.id.rl_results);
		
		txvFgtsValue = (TextView) findViewById(R.id.txv_fgts_value);
		txvMeses = (TextView) findViewById(R.id.txv_meses);
		txvFgtsResult = (TextView) findViewById(R.id.txv_fgts_result);

		hideResultPanels();
		NumberTextWatcher.applyCurrency(edtSalary);
		NumberTextWatcher.applyInteger(edtMeses);

		Validation.applyRequiredValidation(edtSalary);
		Validation.applyRequiredValidation(edtMeses);

		popupFgtsTip = buildPopup(R.layout.popup_tip_content);
		((TextView) popupFgtsTip.getContentView()
				.findViewById(R.id.txv_message)).setText("8,00%");

		hsv = (ScrollView) findViewById(R.id.scroll_container);
		//AdMobUtil.configureAdMob(this, R.id.adv_fgts_activity,null);

		menuDelegate = new MenuDelegate(this);

	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		adService.onDestroy();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//adService.showInterstitialAd();
		adService.onPause();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		adService.showInterstitialAd();
	}

	@Override
	protected void onResume() {
		super.onResume();
		History history = (History) getIntent().getSerializableExtra("history");
		String historyData = (String) getIntent()
				.getStringExtra("history_data");
		if (history != null) {
			SaldoFgtsDataParser parser = new SaldoFgtsDataParser(
					historyData);
			edtSalary.setText(String.valueOf(BigDecimal.valueOf(history.amount ).multiply(BigDecimal.TEN).doubleValue()));
			if (parser.getMeses() != null) {
				edtMeses.setText(String.valueOf(parser.getMeses()));
			}

			this.saldoFgtsPresenter.performBtnCalcClick(history.amount,parser.getMeses(),
					false);
		}
		
		adService.onResume();

	}

	public void hideResultPanels() {
		rlResults.setVisibility(View.GONE);
	}

	public void showResultPanels() {
		rlResults.setVisibility(View.VISIBLE);
		this.handler.post(new Runnable() {
			public void run() {
				hsv.smoothScrollBy(0, rlResults.getTop());
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return menuDelegate.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return menuDelegate.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imv_tip_fgts:
			showFgtsTip();
			break;
		case R.id.txv_fgts:
			showFgtsTip();
			break;
		case R.id.btn_calc:
			onBtnCalcClick();
			break;
		case R.id.btn_clear:
			onBtnClearClick();
		default:
			break;
		}
	}

	private void onBtnClearClick() {

		saldoFgtsPresenter.performBtnClearClick();
	}

	public void clearFields() {
		edtMeses.setText("");
		edtSalary.setText("");
		edtSalary.requestFocus();
	}

	private void onBtnCalcClick() {
		Double salary = 0d;
		int meses =0;
		if (checkValidation()) {
			salary = NumberUtils.parseCurrency(edtSalary.getText().toString());
			meses = NumberUtils.parseInteger(edtMeses.getText().toString());

			this.saldoFgtsPresenter.performBtnCalcClick(salary, meses, true);
			showResultPanels();
		}
	}

	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.hasText(edtSalary)
				|| NumberUtils.parseCurrency(edtSalary.getText().toString()) <= 0) {
			edtSalary.setError(Validation.REQUIRED_MSG);
			ret = false;
		}
		
		if (!Validation.hasText(edtMeses)
				|| NumberUtils.parseInteger(edtMeses.getText().toString()) <= 0) {
			edtMeses.setError(Validation.REQUIRED_MSG);
			ret = false;
		}

		return ret;
	}

	private void showFgtsTip() {
		popupFgtsTip.showAsDropDown(imvFgtsTip);
	}

	private PopupWindow buildPopup(int content) {
		View popupView = getLayoutInflater().inflate(content, null);

		PopupWindow popupTip = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		popupTip.setFocusable(true);
		popupTip.setOutsideTouchable(true);
		popupTip.setBackgroundDrawable(new BitmapDrawable());
		return popupTip;
	}

	public void setFgtsMes(double doubleValue) {

		txvFgtsValue.setText(NumberUtils.formatCurrency(doubleValue));
	}

	public void setMeses(int meses) {

		txvMeses.setText(String.valueOf(meses));
	}

	public void setSaldo(double doubleValue) {

		txvFgtsResult.setText(NumberUtils.formatCurrency(doubleValue));
	}

}
