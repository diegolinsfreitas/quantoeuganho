package diego.freitas.quantoeuganho;

import android.content.Context;
import android.os.Build;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;

public class GATracker {

	public static final String ACCEPT = "accept";
	public static final String LATER = "later";
	public static final String NEVER = "never";

	private static final String PROPERTY_ID = "UA-60309617-1";

	private Tracker tracker;

	public GATracker(Context ctx) {
		if(!Build.SERIAL.equals("04e08bed59ccd0e")){
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(ctx);
			analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
			tracker = analytics.newTracker(PROPERTY_ID);
			tracker.enableAdvertisingIdCollection(true);
			tracker.enableAutoActivityTracking(true);
			tracker.enableExceptionReporting(true);
			tracker.set("DEVICE_ID", Build.SERIAL);
			tracker.setAnonymizeIp(false);
		}
	}
	
	public void recomendEvent(String action){
		if(tracker != null){
			HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
		    .setCategory("ReviewApp")
		    .setAction(action);
			tracker.send(builder.build());
		}
	}

	public void invites(int i, String[] ids) {
		if(tracker != null){
			HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
		    .setCategory("AppInvites")
		    .setAction("invites")
		    .setLabel(String.valueOf(i))
		    .setValue(ids.length);
			tracker.send(builder.build());
		}
	}
	
	public void group(int i) {
		if(tracker != null){
			HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
		    .setCategory("AppInvites")
		    .setAction("invite_groups")
		    .setLabel(String.valueOf(i));
			tracker.send(builder.build());
		}
	}

	public void sendInviteConversion() {
		if(tracker != null){
			HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
		    .setCategory("AppInvites")
		    .setAction("conversion");
			tracker.send(builder.build());
		}
	}

}
