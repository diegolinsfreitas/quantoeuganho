package diego.freitas.quantoeuganho.model;

import java.io.Serializable;

/*
 * Cr�dito do tipo Depostio da empresa implementar somando no saldo existente, (validar essa regra)
 * cr�dito do tipoi jam atualiza com o saldo da mensagem
 */
public class CreditoFGTS implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CreditoFGTS(String date, String lancamento, String amount) {
		super();
		this.date = date;
		this.lancamento = lancamento;
		this.amount = amount;
	}
	
	public CreditoFGTS() {
		// TODO Auto-generated constructor stub
	}
	public String date;
	public String lancamento = "";
	public String amount = "";
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result
				+ ((lancamento == null) ? 0 : lancamento.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CreditoFGTS other = (CreditoFGTS) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (lancamento == null) {
			if (other.lancamento != null)
				return false;
		} else if (!lancamento.equals(other.lancamento))
			return false;
		return true;
	}
	
	
}
