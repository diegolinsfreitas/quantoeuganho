package diego.freitas.quantoeuganho.model;

import java.io.Serializable;
import java.util.Date;

import org.json.JSONObject;

public class History implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -602963285400473449L;
	public int id;
	public CalcType type;
	public transient JSONObject data;
	public Date created;
	public Double amount;

}
