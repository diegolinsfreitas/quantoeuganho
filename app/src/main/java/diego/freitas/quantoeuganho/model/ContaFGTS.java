package diego.freitas.quantoeuganho.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ContaFGTS implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public long id;
	public String accountNumber;
	public String saldo = "";
	public List<CreditoFGTS> creditos = new ArrayList<CreditoFGTS>();

	public String empresa;
	//A - Ativo  I - Inativo
	public String status;
}
