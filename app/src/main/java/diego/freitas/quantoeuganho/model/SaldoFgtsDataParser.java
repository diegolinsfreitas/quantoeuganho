package diego.freitas.quantoeuganho.model;

import org.json.JSONException;
import org.json.JSONObject;

public class SaldoFgtsDataParser {

	public static final String MESES = "meses";


	private JSONObject data = new JSONObject();

	public SaldoFgtsDataParser(String historyData) {
		try {
			this.data = new JSONObject(historyData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated constructor stub
	}

	public Integer getMeses() {
		if (data.has(MESES)) {
			try {
				return data.getInt(MESES);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	
}
