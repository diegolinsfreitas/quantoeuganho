package diego.freitas.quantoeuganho.model;

import org.json.JSONException;
import org.json.JSONObject;

public class SalaryLiquidDataParser {

	public static final String OTHER_DISCOUNTS = "otherDiscounts";
	public static final String BENEFITS = "benefits";
	public static final String DEPENDENTS = "dependents";

	private JSONObject data = new JSONObject();

	public SalaryLiquidDataParser(String historyData) {
		setData(historyData);
	}
	
	public SalaryLiquidDataParser() {
		// TODO Auto-generated constructor stub
	}
	
	public void setData(String historyData){
		try {
			this.data = new JSONObject(historyData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated constructor stub
	}

	public Integer getDependents() {
		if (data.has(DEPENDENTS)) {
			try {
				return data.getInt(DEPENDENTS);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public Double getBenefit() {
		if (data.has(BENEFITS)) {
			try {
				return data.getDouble(BENEFITS);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public Double getDiscounts() {
		if (data.has(OTHER_DISCOUNTS)) {
			try {
				return data.getDouble(OTHER_DISCOUNTS);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

}
