package diego.freitas.quantoeuganho.model;

import org.json.JSONException;
import org.json.JSONObject;

public class DecimoTerceiroDataParser {

	public static final String MONTHS = "months";
	public static final String PARCELA = "parcela";
	public static final String DEPENDENTS = "dependents";

	private JSONObject data = new JSONObject();

	public DecimoTerceiroDataParser(String historyData) {
		setData(historyData);
	}
	
	public DecimoTerceiroDataParser() {
		// TODO Auto-generated constructor stub
	}
	
	public void setData(String historyData){
		try {
			this.data = new JSONObject(historyData);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated constructor stub
	}

	public Integer getDependents() {
		if (data.has(DEPENDENTS)) {
			try {
				return data.getInt(DEPENDENTS);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public int getMonths() {
		if (data.has(MONTHS)) {
			try {
				return data.getInt(MONTHS);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return 11;
	}
	

	public int getParcela() {
		if (data.has(PARCELA)) {
			try {
				return data.getInt(PARCELA);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return 0;
	}

}
