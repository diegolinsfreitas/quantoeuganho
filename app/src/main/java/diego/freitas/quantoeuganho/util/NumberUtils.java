package diego.freitas.quantoeuganho.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import android.content.Context;

public class NumberUtils {
	
	
	private static final Locale LOCALE = new Locale("pt","BR");

	public static String formatCurrency(Double money){
		NumberFormat formatter = NumberFormat.getCurrencyInstance(LOCALE);
		return formatter.format(money);
	}
	
	public static Double parseCurrency(String money){
		if(money == null || money.trim().equalsIgnoreCase("")){
			return 0d;
		}
		NumberFormat formatter = NumberFormat.getCurrencyInstance(LOCALE);
		try {
			return formatter.parse(money).doubleValue();
		} catch (ParseException e) {
			return 0d;
		}
	}
	
	public static Double parsePercent(String money){
		if(money == null || money.trim().equalsIgnoreCase("")){
			return 0d;
		}
		NumberFormat formatter = NumberFormat.getPercentInstance(LOCALE);
		try {
			return formatter.parse(money).doubleValue();
		} catch (ParseException e) {
			return 0d;
		}
	}

	public static Double parseNumber(String value) {
		if(value == null || value.trim().equalsIgnoreCase("")){
			return 0d;
		}
		NumberFormat formatter = NumberFormat.getNumberInstance(LOCALE);
		try {
			return formatter.parse(value).doubleValue();
		} catch (ParseException e) {
			return 0d;
		}
	}
	
	public static Integer parseInteger(String value) {
		if(value == null || value.trim().equalsIgnoreCase("")){
			return 0;
		}
		NumberFormat formatter = NumberFormat.getIntegerInstance(LOCALE);
		try {
			return formatter.parse(value).intValue();
		} catch (ParseException e) {
			return 0;
		}
	}

	public static String formatDate(Context ctx, Date created) {
		  DateFormat dateInstance = DateFormat.getDateInstance(DateFormat.MEDIUM, LOCALE);
		return dateInstance.format(created);
	}

	public static String formatPercent(double d) {
		NumberFormat percentInstance = NumberFormat.getPercentInstance(LOCALE);
		
		percentInstance.setMaximumFractionDigits(2);
		percentInstance.setMaximumIntegerDigits(3);
		return percentInstance.format(d);
	}

	public static String formatSeguroRequerimento(String raw) {
		return String.format("%s.%s.%s-%s", raw.substring(0,1),raw.substring(1,4),raw.substring(4,10),raw.substring(10));
	}

}
