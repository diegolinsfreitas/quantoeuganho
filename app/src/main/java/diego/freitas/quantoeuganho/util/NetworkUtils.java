package diego.freitas.quantoeuganho.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import diego.freitas.quantoeuganho.QuantoEuGanhoApplication;

/**
 * Created by diego on 14/02/16.
 */
public class NetworkUtils {
    public static  boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) QuantoEuGanhoApplication.instance.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = cm.getActiveNetworkInfo();

        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        if (!i.isAvailable())
            return false;

        return true;
    }
}
