package diego.freitas.quantoeuganho.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.widget.EditText;

public class NumberTextWatcher implements TextWatcher {

	private EditText et;

	public NumberTextWatcher(EditText et, FormatterHelper formaterHelper) {

		this.et = et;
		formatter = formaterHelper;

	}

	@SuppressWarnings("unused")
	private static final String TAG = "NumberTextWatcher";

	public void afterTextChanged(Editable s) {
		
	}

	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}

	String current = "";
	private FormatterHelper formatter;

	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if (!s.toString().equals(current)) {
			et.removeTextChangedListener(this);

			String formated = formatter.format(s);

			current = formated;
			et.setText(formated);
			et.setSelection(formated.length());

			et.addTextChangedListener(this);
		}
	}

	public static interface FormatterHelper {
		String format(CharSequence s);
	}

	private static class CurrencyFormatterHelper implements FormatterHelper {
		public String format(CharSequence s) {
			String cleanString = s.toString().replaceAll("[R$,.]", "");

			double parsed = Double
					.parseDouble("".equalsIgnoreCase(cleanString) ? "0"
							: cleanString);
			NumberFormat formatter = NumberFormat
					.getCurrencyInstance(new Locale("pt", "BR"));
			String formated = formatter.format((parsed / 100));
			return formated;
		}
	}

	private static class NumberFormatterHelper implements FormatterHelper {
		public String format(CharSequence s) {
			String cleanString = s.toString().replaceAll("[,.]", "");
			double parsed = Double
					.parseDouble("".equalsIgnoreCase(cleanString) ? "0"
							: cleanString);
			NumberFormat nf = NumberFormat.getNumberInstance();
			DecimalFormat df = (DecimalFormat) nf;
			df.applyPattern("###.#");
			return df.format(parsed / 10 + 0.01);
		}
	}

	private static class IntegerFormatterHelper implements FormatterHelper {
		public String format(CharSequence s) {
			String cleanString = s.toString().replaceAll("[.,]", "");

			long parsed = Long
					.parseLong("".equalsIgnoreCase(cleanString) ? "0"
							: cleanString);
			NumberFormat formatter = NumberFormat
					.getIntegerInstance(new Locale("pt", "BR"));
			String formated = formatter.format(parsed);
			return formated;
		}
	}

	private static class PercentFormatterHelper implements FormatterHelper {
		private CurrencyFormatterHelper currencyFormatterHelper;

		public String format(CharSequence s) {
			
			currencyFormatterHelper = new CurrencyFormatterHelper();
			return currencyFormatterHelper.format(s).replace("R$", "");
		}
	}

	public static void applyCurrency(EditText edtText) {
		edtText.addTextChangedListener(new NumberTextWatcher(edtText,
				new CurrencyFormatterHelper()));
	}

	public static void applyNumber(EditText edtText) {
		edtText.addTextChangedListener(new NumberTextWatcher(edtText,
				new NumberFormatterHelper()));
	}

	public static void applyPercent(EditText edtText) {
		edtText.addTextChangedListener(new NumberTextWatcher(edtText,
				new PercentFormatterHelper()));
	}

	public static void applyInteger(EditText edtText) {
		edtText.addTextChangedListener(new NumberTextWatcher(edtText,
				new IntegerFormatterHelper()));
	}
}