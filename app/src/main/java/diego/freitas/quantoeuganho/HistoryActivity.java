package diego.freitas.quantoeuganho;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import diego.freitas.quantoeuganho.model.History;
import diego.freitas.quantoeuganho.persistence.HistoryDAO;
import diego.freitas.quantoeuganho.util.FontUtils;

public class HistoryActivity extends FragmentActivity implements OpenCalcAction {

	private ListView lstHistory;
	private HistoryAdapter historyAdapter;
	private HistoryDAO historyDAO;
	private MenuDelegate menuDelegate;
	private AdService adService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);
		adService = new AdService(this);
		adService.loadAd(this);
		lstHistory = (ListView) findViewById(R.id.lst_history);
		TextView emptyView = new TextView(this);

		emptyView.setText("Sem histórico de cálculos...");

		
		emptyView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);

		android.widget.RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
				new LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT));
		layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,
				RelativeLayout.TRUE);
		emptyView.setLayoutParams(layoutParams);
		((ViewGroup) lstHistory.getParent()).addView(emptyView);
		lstHistory.setEmptyView(emptyView);
		historyAdapter = new HistoryAdapter();
		historyAdapter.layoutInflater = getLayoutInflater();
		historyDAO = new HistoryDAO();
		historyAdapter.openCalcAction = this;
		//AdMobUtil.configureAdMob(this, R.id.adv_history_activity,null);
		menuDelegate = new MenuDelegate(this);
		FontUtils.overrideFonts(this, FontUtils.ROBOTO_LIGHT, emptyView);
		FontUtils.overrideFonts(this, FontUtils.ROBOTO_LIGHT,
				findViewById(R.id.main_activity_container));
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		adService.onDestroy();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//adService.showInterstitialAd();
		adService.onPause();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		adService.showInterstitialAd();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return menuDelegate.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return menuDelegate.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		historyAdapter.data = historyDAO.getHistory();
		lstHistory.setAdapter(historyAdapter);
		adService.onResume();
	}

	@Override
	public void open(History history) {
		Intent intent = null;
		switch (history.type) {
		case SALARY_LIQUID:
			intent = new Intent(this, SalarioLiquidoActivity.class);
			break;
		case FGTS:
			intent = new Intent(this, SaldoFgtsActivity.class);
			break;
		case DECIMO_TERCEIRO:
			intent = new Intent(this, DecimoTerceiroActivity.class);
			break;
		default:
			break;
		}
		intent.putExtra("history", history);
		intent.putExtra("history_data", history.data.toString());
		startActivity(intent);
	}

}
