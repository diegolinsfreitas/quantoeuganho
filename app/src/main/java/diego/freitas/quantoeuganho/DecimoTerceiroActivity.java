package diego.freitas.quantoeuganho;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.MultipleCategorySeries;
import org.achartengine.renderer.DialRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;
import diego.freitas.quantoeuganho.model.DecimoTerceiroDataParser;
import diego.freitas.quantoeuganho.model.History;
import diego.freitas.quantoeuganho.presenter.DecimoTerceiroPresenter;
import diego.freitas.quantoeuganho.util.FontUtils;
import diego.freitas.quantoeuganho.util.NumberTextWatcher;
import diego.freitas.quantoeuganho.util.NumberUtils;
import diego.freitas.quantoeuganho.util.Validation;

public class DecimoTerceiroActivity extends FragmentActivity implements
		OnClickListener {

	private View imvInssTip;
	private View txvInss;
	private View btnCalc;
	private EditText edtSalary;

	private EditText edtDepenents;

	private DecimoTerceiroPresenter decimoTerceiroPresenter;
	private TextView txvSalBrutoValue;

	private TextView txvInssValue;
	private TextView txvIrrfValue;

	private TextView txvProventosValue;
	private TextView txvDescontos;
	private TextView txvSalResult;
	private PopupWindow popupInssTip;
	private PopupWindow popupIrrfTip;
	private View imvIrrfTip;
	private View txvIrrf;
	private View btnClear;
	private ViewGroup rlGraph;
	private View rlResults;
	private ScrollView hsv;
	private Handler handler = new Handler();
	private diego.freitas.quantoeuganho.MenuDelegate menuDelegate;

	private GraphicalView mChart;
	private LinearLayout llChart;
	
	private int[] CHART_SERIES_COLORS= new int[]{R.color.chart_serie1,R.color.chart_serie2,R.color.chart_serie3,R.color.chart_serie4,R.color.chart_serie5};
	private Spinner spnMonths;
	
	private int selectedParcela;
	private TextView txvDecimo;
	private TextView txvDecimoLabel;
	private TextView txvPrimeiraParcela;
	private TextView txvPrimeiraParcelaLabel;
	private TextView txvLiquidLabel;
	private AdService adService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_decimo_terceiro);
		adService = new AdService(this);
		adService.loadAd(this);
		decimoTerceiroPresenter = new DecimoTerceiroPresenter(this);


		FontUtils.overrideFonts(this, FontUtils.ROBOTO_LIGHT,
				findViewById(R.id.scroll_container));

		imvInssTip = findViewById(R.id.imv_tip_inss);
		txvInss = findViewById(R.id.txv_inss);
		txvInss.setOnClickListener(this);
		imvInssTip.setOnClickListener(this);

		imvIrrfTip = findViewById(R.id.imv_tip_irrf);
		txvIrrf = findViewById(R.id.txv_irrf);
		txvIrrf.setOnClickListener(this);
		imvIrrfTip.setOnClickListener(this);
		txvDecimoLabel = (TextView) findViewById(R.id.txv_decimo);
		txvDecimo = (TextView) findViewById(R.id.txv_decimo_value);
		txvPrimeiraParcela = (TextView) findViewById(R.id.txv_primeira_parcela_value);
		txvPrimeiraParcelaLabel = (TextView) findViewById(R.id.txv_primeira_parcela);
		txvLiquidLabel = (TextView) findViewById(R.id.txv_liquido);
	

		edtSalary = (EditText) findViewById(R.id.edt_brute_salary);
		spnMonths = (Spinner) findViewById(R.id.spn_meses);
		spnMonths.setAdapter(new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item,new Integer[]{1,2,3,4,5,6,7,8,9,10,11,12}));
		edtDepenents = (EditText) findViewById(R.id.edt_n_dependents);

		btnCalc = findViewById(R.id.btn_calc);
		btnCalc.setOnClickListener(this);
		btnClear = findViewById(R.id.btn_clear);
		btnClear.setOnClickListener(this);

		rlGraph = (ViewGroup) findViewById(R.id.rl_graph);
		rlResults = findViewById(R.id.rl_results);

		hideResultPanels();
		NumberTextWatcher.applyCurrency(edtSalary);
		NumberTextWatcher.applyInteger(edtDepenents);
		
		((RadioGroup) findViewById(R.id.tgl_parcelas)).setOnCheckedChangeListener(ToggleListener);  

		Validation.applyRequiredValidation(edtSalary);

		popupInssTip = buildPopup(R.layout.popup_tip_content);
		popupIrrfTip = buildPopup(R.layout.popup_tip_content);

		hsv = (ScrollView) findViewById(R.id.scroll_container);

		menuDelegate = new MenuDelegate(this);
		llChart = (LinearLayout) findViewById(R.id.ll_chart);

	}

	@Override
	protected void onResume() {
		super.onResume();
		adService.onResume();
		
		selectedParcela = 0;
		spnMonths.setSelection(11);
		
		History history = (History) getIntent().getSerializableExtra("history");
		String historyData = (String) getIntent()
				.getStringExtra("history_data");
		if (history != null) {
			DecimoTerceiroDataParser parser = new DecimoTerceiroDataParser(
					historyData);
			edtSalary.setText(String.valueOf(history.amount * 10));
			
			
			selectedParcela = parser.getParcela();
			checkParcela(selectedParcela);

			if (parser.getDependents() != null) {
				edtDepenents.setText(String.valueOf(parser.getDependents()));
			}

			spnMonths.setSelection(parser.getMonths());
			

			this.decimoTerceiroPresenter
					.performBtnCalcClick(
							history.amount,
							parser.getDependents() == null ? 0 : parser
									.getDependents(),
							 parser
									.getMonths()+1,selectedParcela, false);
		}
		
		

	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		adService.onDestroy();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//adService.showInterstitialAd();
		adService.onPause();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		adService.showInterstitialAd();
	}

	private void checkParcela(int parcela) {
		switch (parcela) {
		case 0:
			((ToggleButton) findViewById(R.id.btn_unica)).setChecked(true);
			((ToggleButton) findViewById(R.id.btn_primeira)).setChecked(false);
			((ToggleButton) findViewById(R.id.btn_segunda)).setChecked(false);
			break;
		case 1:
			((ToggleButton) findViewById(R.id.btn_unica)).setChecked(false);
			((ToggleButton) findViewById(R.id.btn_primeira)).setChecked(true);
			((ToggleButton) findViewById(R.id.btn_segunda)).setChecked(false);
			break;
		case 2:
			((ToggleButton) findViewById(R.id.btn_unica)).setChecked(false);
			((ToggleButton) findViewById(R.id.btn_primeira)).setChecked(false);
			((ToggleButton) findViewById(R.id.btn_segunda)).setChecked(true);
			break;

		default:
			break;
		}
	}

	public void hideResultPanels() {
		rlGraph.setVisibility(View.GONE);
		rlResults.setVisibility(View.GONE);
	}

	public void showResultPanels() {
		rlGraph.setVisibility(View.VISIBLE);
		rlResults.setVisibility(View.VISIBLE);
		this.handler.post(new Runnable() {
			public void run() {
				hsv.smoothScrollBy(0, rlResults.getTop());
			}
		});

	}

	public void updateGraph(String[] labels, double[] data) {

		MultipleCategorySeries category = new MultipleCategorySeries(
				"Distribuição Salário");
		

		final DialRenderer renderer = new DialRenderer();
		renderer.setZoomEnabled(false);
		renderer.setExternalZoomEnabled(false);
		renderer.setTextTypeface(FontUtils.getTypeFaceByName(this,
				FontUtils.ROBOTO_LIGHT));
		renderer.setLabelsTextSize(25);
		renderer.setLabelsColor(Color.BLACK);
		renderer.setShowLabels(true);
		renderer.setMinValue(0);
		renderer.setMaxValue(100);
		renderer.setPanEnabled(false);
		renderer.setShowLegend(false);
		SimpleSeriesRenderer r = null; 
		for (int i = 0; i < data.length; i++) {
			r = new SimpleSeriesRenderer();
			r.setColor(getResources().getColor(CHART_SERIES_COLORS[i]));
			renderer.addSeriesRenderer(r);
			labels[i] = labels[i]+" " + NumberUtils.formatPercent(data[i]/100); 
		}

		category.add(labels,data);
		

		if (mChart != null) {
			llChart.removeView(mChart);
		}
		mChart = ChartFactory.getDoughnutChartView(this, category, renderer);
		mChart.setBackgroundColor(Color.WHITE);
		llChart.addView(mChart);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return menuDelegate.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return menuDelegate.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imv_tip_inss:
			showInssTip();
			break;
		case R.id.txv_inss:
			showInssTip();
			break;
		case R.id.imv_tip_irrf:
			showIrrfTip();
			break;
		case R.id.txv_irrf:
			showIrrfTip();
			break;
		case R.id.btn_calc:
			onBtnCalcClick();
			break;
		case R.id.btn_clear:
			onBtnClearClick();
		default:
			break;
		}
	}

	private void onBtnClearClick() {

		decimoTerceiroPresenter.performBtnClearClick();
	}

	public void clearFields() {
		edtDepenents.setText("");
		edtSalary.setText("");
		selectedParcela = 0;
		checkParcela(selectedParcela);
		spnMonths.setSelection(11);
		((ToggleButton) findViewById(R.id.btn_unica)).setChecked(true);
	}

	private void onBtnCalcClick() {
		int dependents = 0;
		Double salary = 0d;
		Integer months = 1;
		if (checkValidation()) {
			salary = NumberUtils.parseCurrency(edtSalary.getText().toString());

			dependents = NumberUtils.parseInteger(edtDepenents.getText()
					.toString());
			months =(Integer) spnMonths.getSelectedItem();
			
			
			this.decimoTerceiroPresenter.performBtnCalcClick(salary,
					dependents, months,selectedParcela, true);

		}
	}

	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.hasText(edtSalary)
				|| NumberUtils.parseCurrency(edtSalary.getText().toString()) <= 0) {
			edtSalary.setError(Validation.REQUIRED_MSG);
			ret = false;
		}

		return ret;
	}

	private void showInssTip() {
		popupInssTip.showAsDropDown(imvInssTip);
	}

	private void showIrrfTip() {
		popupIrrfTip.showAsDropDown(imvIrrfTip);
	}

	private PopupWindow buildPopup(int content) {
		View popupView = getLayoutInflater().inflate(content, null);

		PopupWindow popupTip = new PopupWindow(popupView,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		popupTip.setFocusable(true);
		popupTip.setOutsideTouchable(true);
		popupTip.setBackgroundDrawable(new BitmapDrawable());
		return popupTip;
	}

	public void setSalaryResult(double doubleValue) {
		txvSalBrutoValue = (TextView) findViewById(R.id.txv_sal_bruto_value);
		txvSalBrutoValue.setText(NumberUtils.formatCurrency(doubleValue));
	}

	public void setInssResult(Double doubleValue) {
		txvInssValue = (TextView) findViewById(R.id.txv_inss_value);
		if(doubleValue == null){
			txvInssValue.setText("--");
		}else {
			txvInssValue.setText(NumberUtils.formatCurrency(doubleValue));	
		}
		
	}

	public void setIrrfResult(Double doubleValue) {
		txvIrrfValue = (TextView) findViewById(R.id.txv_irrf_value);
		if(doubleValue == null){
			txvIrrfValue.setText("--");
		} else {
			txvIrrfValue.setText(NumberUtils.formatCurrency(doubleValue));
		}
		
	}

	public void setDecimoTerceiro(Double decimo, int months){
		txvDecimo.setText(NumberUtils.formatCurrency(decimo));
		txvDecimoLabel.setText(String.format("13º (%d meses)", months));
	}
	public void setProventos(Double doubleValue) {
		txvProventosValue = (TextView) findViewById(R.id.txv_proventos);
		txvProventosValue.setText(NumberUtils.formatCurrency(doubleValue));
	}

	public void setDicounts(Double doubleValue) {
		txvDescontos = (TextView) findViewById(R.id.txv_descontos);
		if(doubleValue == null){
			txvDescontos.setText("--");
		} else {
			txvDescontos.setText(NumberUtils.formatCurrency(doubleValue));
		}
	}

	public void setLiquid(Double doubleValue) {
		txvSalResult = (TextView) findViewById(R.id.txv_sal_result);
		txvSalResult.setText(NumberUtils.formatCurrency(doubleValue));
	}

	public void setIrrfRate(String rate) {

		((TextView) popupIrrfTip.getContentView()
				.findViewById(R.id.txv_message)).setText(rate);
	}

	public void setInssRate(String rate) {
		((TextView) popupInssTip.getContentView()
				.findViewById(R.id.txv_message)).setText(rate);

	}
	
	public void setPrimeiraParcela(Double primeiraParcela){
		txvPrimeiraParcela.setText(NumberUtils.formatCurrency(primeiraParcela));
	}
	
	public void onToogleParcela(View v){
		((RadioGroup)v.getParent()).check(v.getId());
		switch (v.getId()) {
		case R.id.btn_unica:
			selectedParcela = 0;			
			break;
		case R.id.btn_primeira:
			selectedParcela = 1;
			break;
		case R.id.btn_segunda:
			selectedParcela = 2;
			break;
		default:
			break;
		}
		checkParcela(selectedParcela);
		hideResultPanels();
	}
	
	static final RadioGroup.OnCheckedChangeListener ToggleListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(final RadioGroup radioGroup, final int i) {
            for (int j = 0; j < radioGroup.getChildCount(); j++) {
                final ToggleButton view = (ToggleButton) radioGroup.getChildAt(j);
                view.setChecked(view.getId() == i);
            }
        }
    };

	public void hidePrimeiraParcela() {
		// TODO Auto-generated method stub
		this.txvPrimeiraParcela.setVisibility(View.GONE);
		this.txvPrimeiraParcelaLabel.setVisibility(View.GONE);
	}
	
	public void showPrimeiraParcela() {
		this.txvPrimeiraParcela.setVisibility(View.VISIBLE);
		this.txvPrimeiraParcelaLabel.setVisibility(View.VISIBLE);
	}

	public void setLiquidLabel(String string) {
		
		txvLiquidLabel.setText(string);
	}

	public void hideCalcView() {
		findViewById(R.id.txv_proventos).setVisibility(View.GONE);
		findViewById(R.id.txv_descontos).setVisibility(View.GONE);
		findViewById(R.id.txv_sub_signal).setVisibility(View.GONE);
		findViewById(R.id.sub_divider).setVisibility(View.GONE);
	}
	
	public void showCalcView() {
		findViewById(R.id.txv_proventos).setVisibility(View.VISIBLE);
		findViewById(R.id.txv_descontos).setVisibility(View.VISIBLE);
		findViewById(R.id.txv_sub_signal).setVisibility(View.VISIBLE);
		findViewById(R.id.sub_divider).setVisibility(View.VISIBLE);
	}
}
