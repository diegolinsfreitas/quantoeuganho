package diego.freitas.quantoeuganho;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

public class TutorialNisActivity extends FragmentActivity implements TutorialListener{
	
	/**
	 * The number of pages (wizard steps) to show in this demo.
	 */
	private static final int NUM_PAGES = 2;

	/**
	 * The pager widget, which handles animation and allows swiping horizontally
	 * to access previous and next wizard steps.
	 */
	private ViewPager mPager;

	/**
	 * The pager adapter, which provides the pages to the view pager widget.
	 */
	private PagerAdapter mPagerAdapter;

	private PreferencesManager preferencesManager;

	private String service;

	private AdService adService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tutorial_nis_slide);
		
		// Instantiate a ViewPager and a PagerAdapter.
		mPager = (ViewPager) findViewById(R.id.pager);
		mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
		mPager.setAdapter(mPagerAdapter);
		
		preferencesManager = new PreferencesManager(this);
		
		service = getIntent().getExtras().getString("service");
		
		adService = new AdService(this);
		adService.loadAd(this);
	}

	@Override
	public void onBackPressed() {
		if (mPager.getCurrentItem() == 0) {
			// If the user is currently looking at the first step, allow the
			// system; to handle the
			// Back button. This calls finish() on this activity and pops the
			// back stack.
			super.onBackPressed();
			setActivityResult();
			preferencesManager.tutorialUnderstood();
		} else {
			// Otherwise, select the previous step.
			mPager.setCurrentItem(mPager.getCurrentItem() - 1);
		}
	}

	private void setActivityResult() {
		Intent data = new Intent();
		data.putExtra("service", service);
		this.setResult(Activity.RESULT_OK, data);
	}

	/**
	 * A simple pager adapter that represents 5 ScreenSlidePageFragment objects,
	 * in sequence.
	 */
	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
		public ScreenSlidePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;
			switch (position) {
			case 0:
				fragment = new TutorialNISFragment();
				break;
			case 1:
				fragment =  new TutorialPasswordFragment();
				break;
			default:
				fragment = new TutorialNISFragment();
			}

			return fragment;
		}

		@Override
		public int getCount() {
			return NUM_PAGES;
		}
	}

	@Override
	public void close() {
		passwordOk();
	}

	@Override
	public void nisOk() {
		mPager.setCurrentItem(1);
	}

	@Override
	public void passwordOk() {
		preferencesManager.tutorialUnderstood();
		setActivityResult();
		
		this.finish();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//adService.showInterstitialAd();
		adService.onPause();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		adService.onResume();
	}
}