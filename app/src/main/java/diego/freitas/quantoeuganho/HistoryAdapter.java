package diego.freitas.quantoeuganho;

import java.util.List;

import org.json.JSONException;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import diego.freitas.quantoeuganho.model.DecimoTerceiroDataParser;
import diego.freitas.quantoeuganho.model.History;
import diego.freitas.quantoeuganho.model.SalaryLiquidDataParser;
import diego.freitas.quantoeuganho.model.SaldoFgtsDataParser;
import diego.freitas.quantoeuganho.persistence.HistoryDAO;
import diego.freitas.quantoeuganho.util.FontUtils;
import diego.freitas.quantoeuganho.util.NumberUtils;

public class HistoryAdapter extends BaseAdapter {

	List<History> data;
	LayoutInflater layoutInflater;

	HistoryDAO history = new HistoryDAO();

	int[] iconTypes = new int[] { R.drawable.payment, R.drawable.agreement,
			R.drawable.fund, R.drawable.calendar};

	int[] titleTypes = new int[] { R.string.type_salario_liquido,
			R.string.type_recisao, R.string.type_saldo_fgts, R.string.type_decimo_terceiro};
	OpenCalcAction openCalcAction;
	private SalaryLiquidDataParser sp = new SalaryLiquidDataParser();
	
	private DecimoTerceiroDataParser dp = new DecimoTerceiroDataParser();

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return data.get(position).id;
	}

	@Override
	public View getView(int index, View v, ViewGroup vGroup) {
		HistoryViewHolder viewHolder;

		if (v == null) {
			v = layoutInflater.inflate(R.layout.history_item, vGroup, false);
			FontUtils.overrideFonts(layoutInflater.getContext(),
					FontUtils.ROBOTO_LIGHT, v);
			viewHolder = new HistoryViewHolder();
			viewHolder.txvTitle = (TextView) v.findViewById(R.id.txv_title);
			viewHolder.imgType = (ImageView) v.findViewById(R.id.imv_icon_type);
			viewHolder.txvValue = (TextView) v.findViewById(R.id.txv_amount);
			viewHolder.txvCreated = (TextView) v.findViewById(R.id.txv_date);
			viewHolder.txvInfo = (TextView) v.findViewById(R.id.txv_info);
			viewHolder.imvDelete = (ImageView) v.findViewById(R.id.imv_action);
			v.setTag(viewHolder);
		} else {
			viewHolder = (HistoryViewHolder) v.getTag();
		}

		final History history = (History) getItem(index);
		if (history != null) {
			loadView(viewHolder, history, index);
		}

		v.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				openCalcAction.open(history);
			}
		});
		return v;
	}

	private void loadView(HistoryViewHolder viewHolder, final History history,
			final int index) {
		viewHolder.txvTitle.setText(titleTypes[history.type.ordinal()]);
		viewHolder.imgType.setImageResource(iconTypes[history.type.ordinal()]);
		viewHolder.txvValue.setText(NumberUtils.formatCurrency(history.amount));
		viewHolder.txvCreated.setText(NumberUtils.formatDate(
				layoutInflater.getContext(), history.created));
		viewHolder.itemId = history.id;
		viewHolder.imvDelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				HistoryAdapter.this.history.delete(history.id);
				HistoryAdapter.this.data.remove(index);
				HistoryAdapter.this.notifyDataSetChanged();
			}
		});
		viewHolder.txvInfo.setText("");
		switch (history.type) {
		case SALARY_LIQUID:
			loadSalaryInfoView(viewHolder.txvInfo, history);
			break;
		case FGTS:
			loadFGTSInfoView(viewHolder.txvInfo, history);
			break;
		case DECIMO_TERCEIRO:
			loadDecimoTerceiroInfoView(viewHolder.txvInfo, history);
			break;
		default:
			break;
		}
	}

	private void loadFGTSInfoView(TextView txvInfo, History history2) {

		try {
			txvInfo.setText(" Meses: "
					+ history2.data.getString(SaldoFgtsDataParser.MESES) );
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loadSalaryInfoView(TextView txvInfo, History history) {
		StringBuilder bd = new StringBuilder();
		sp.setData(history.data.toString());
		if (sp.getBenefit() != null) {
			bd.append(" Benef.:"
					+ NumberUtils.formatPercent(((sp.getBenefit()/100))));
		}
		if (sp.getDependents() != null) {
			bd.append(" Depend..:" + sp.getDependents());
		}
		if (sp.getDiscounts() != null) {
			bd.append(" Outros.:" + sp.getDiscounts());
		}
		if (bd.length() > 0) {
			txvInfo.setText(bd.toString());
		}
		// viewHolder.txvInfo.setText("Ben:"
		// +NumberUtils.formatCurrency(money)(history.data.getDouble(SalarioLiquidoPresenter.BENEFITS)));
	}
	

	private void loadDecimoTerceiroInfoView(TextView txvInfo, History history) {
		StringBuilder bd = new StringBuilder();
		dp.setData(history.data.toString());
		switch (dp.getParcela()) {
		case 0:
			bd.append(" Parc.: Única");	
			break;
		case 1:
			bd.append(" Parc.: 1ª");	
			break;
		case 2:
			bd.append(" Parc.: 2ª");	
			break;
		default:
			break;
		}
		
		
		if (bd.length() > 0) {
			txvInfo.setText(bd.toString());
		}
	}

	static class HistoryViewHolder {
		public ImageView imvDelete;
		int itemId;
		public TextView txvInfo;
		TextView txvCreated;
		ImageView imgType;
		TextView txvTitle;
		TextView txvValue;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return data.isEmpty();
	}

}
