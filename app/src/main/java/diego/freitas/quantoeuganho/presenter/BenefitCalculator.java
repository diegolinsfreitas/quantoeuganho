package diego.freitas.quantoeuganho.presenter;

import java.math.BigDecimal;

class BenefitCalculator {

	BigDecimal value = BigDecimal.ZERO;

	public void calculate(BigDecimal salary, BigDecimal benefit) {
		if (salary.doubleValue() <= 0) {
			throw new IllegalArgumentException(
					"valor invalido para salario");
		}
		value = salary.multiply(benefit.divide(new BigDecimal(100)));
	}

	public BigDecimal getValue() {
		return value;
	}

}