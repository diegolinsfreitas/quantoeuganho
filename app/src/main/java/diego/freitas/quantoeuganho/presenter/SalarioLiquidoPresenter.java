
package diego.freitas.quantoeuganho.presenter;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.json.JSONException;
import org.json.JSONObject;

import diego.freitas.quantoeuganho.SalarioLiquidoActivity;
import diego.freitas.quantoeuganho.model.CalcType;
import diego.freitas.quantoeuganho.model.History;
import diego.freitas.quantoeuganho.model.SalaryLiquidDataParser;
import diego.freitas.quantoeuganho.persistence.HistoryDAO;

public class SalarioLiquidoPresenter {

	private SalarioLiquidoActivity salarioLiquidoActivity;
	private HistoryDAO historyDAO;

	public SalarioLiquidoPresenter(SalarioLiquidoActivity salarioLiquidoActivity) {
		this.salarioLiquidoActivity = salarioLiquidoActivity;
		historyDAO = new HistoryDAO();
	}

	public void performBtnCalcClick(Double salary, Double benefits,
			Integer dependents, Double otherDiscounts, boolean registerHistory) {
		InssCalculator inssCalculator = new InssCalculator();
		IRRFCalculator irrfCalculator = new IRRFCalculator();
		BenefitCalculator benefitCalculator = new BenefitCalculator();
		BigDecimal salaryForCalc = new BigDecimal(salary);
		inssCalculator.calculate(salaryForCalc);
		benefitCalculator
				.calculate(salaryForCalc, BigDecimal.valueOf(benefits));
		irrfCalculator.calculate(
				salaryForCalc.add(benefitCalculator.getValue()),
				inssCalculator.getValue(), BigDecimal.valueOf(dependents));
		BigDecimal proventos = salaryForCalc.add(benefitCalculator.getValue());

		BigDecimal discounts = BigDecimal.valueOf(otherDiscounts)
				.add(inssCalculator.getValue()).add(irrfCalculator.getValue());

		BigDecimal liquid = proventos.subtract(discounts);

		this.salarioLiquidoActivity
				.setSalaryResult(salaryForCalc.doubleValue());
		this.salarioLiquidoActivity.setBenefitResult(benefitCalculator
				.getValue().doubleValue());
		this.salarioLiquidoActivity.setInssResult(inssCalculator.getValue()
				.doubleValue());
		this.salarioLiquidoActivity.setIrrfResult(irrfCalculator.getValue()
				.doubleValue());
		this.salarioLiquidoActivity.setOtherDiscount(otherDiscounts
				.doubleValue());
		this.salarioLiquidoActivity.setProventos(proventos.doubleValue());
		this.salarioLiquidoActivity.setDicounts(discounts.doubleValue());
		this.salarioLiquidoActivity.setLiquid(liquid.doubleValue());
		DecimalFormat numberFormat = new DecimalFormat("00.00%");

		String inssRate = "";
		if (inssCalculator.istTetoReached()) {
			inssRate = "Teto";
		} else {
			inssRate = numberFormat.format(inssCalculator.rate.doubleValue());
		}
		this.salarioLiquidoActivity.setInssRate(inssRate);

		String irrfRateText = "";
		if (irrfCalculator.rate.equals(BigDecimal.ZERO)) {
			irrfRateText = "Isento";
		} else {
			irrfRateText = numberFormat.format(irrfCalculator.rate
					.doubleValue());
		}

		this.salarioLiquidoActivity.setIrrfRate(irrfRateText);
		
		
		String[] labels=new String[]{"Líquido","Benef.","INSS","IRRF","Outros"};
		double[] data = new double[]{
				liquid.doubleValue()/proventos.doubleValue()*100,
				benefitCalculator.value.doubleValue()/proventos.doubleValue()*100,
				inssCalculator.value.doubleValue()/proventos.doubleValue()*100,
				irrfCalculator.value.doubleValue()/proventos.doubleValue()*100,
				otherDiscounts/proventos.doubleValue()*100
			};
		this.salarioLiquidoActivity.updateGraph(labels, data);

		if (registerHistory) {
			registerHistory(salary, benefits, dependents, otherDiscounts);
		}

		this.salarioLiquidoActivity.showResultPanels();

	}

	private void registerHistory(Double salary, Double benefits,
			Integer dependents, Double otherDiscounts) {
		try {
			History h = new History();
			h.data = new JSONObject();
			if (dependents > 0)
				h.data.put(SalaryLiquidDataParser.DEPENDENTS, dependents);
			if (benefits > 0d)
				h.data.put(SalaryLiquidDataParser.BENEFITS, benefits);
			if (otherDiscounts > 0d)
				h.data.put(SalaryLiquidDataParser.OTHER_DISCOUNTS,
						otherDiscounts);
			h.type = CalcType.SALARY_LIQUID;
			h.amount = salary;
			historyDAO.saveHistory(h);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void performBtnClearClick() {
		this.salarioLiquidoActivity.clearFields();
		this.salarioLiquidoActivity.hideResultPanels();
	}

}
