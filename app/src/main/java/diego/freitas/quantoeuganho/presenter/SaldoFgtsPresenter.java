package diego.freitas.quantoeuganho.presenter;

import java.math.BigDecimal;

import org.json.JSONException;
import org.json.JSONObject;

import diego.freitas.quantoeuganho.SaldoFgtsActivity;
import diego.freitas.quantoeuganho.model.CalcType;
import diego.freitas.quantoeuganho.model.History;
import diego.freitas.quantoeuganho.model.SalaryLiquidDataParser;
import diego.freitas.quantoeuganho.model.SaldoFgtsDataParser;
import diego.freitas.quantoeuganho.persistence.HistoryDAO;

public class SaldoFgtsPresenter {

	private SaldoFgtsActivity saldoFgtsActivity;
	private HistoryDAO historyDAO;

	public SaldoFgtsPresenter(SaldoFgtsActivity saldoFgtsActivity) {
		this.saldoFgtsActivity = saldoFgtsActivity;
		historyDAO = new HistoryDAO();
	}

	public void performBtnCalcClick(Double salary, Integer months, boolean registerHistory ) {
		BigDecimal salaryForCalc = new BigDecimal(salary);

		BigDecimal fgtsMes = salaryForCalc.multiply(BigDecimal.valueOf(8d/100));
		BigDecimal saldoFgts = fgtsMes.multiply(BigDecimal.valueOf(months));

		
		this.saldoFgtsActivity.setFgtsMes(fgtsMes.doubleValue());
		this.saldoFgtsActivity.setMeses(months);
		
		this.saldoFgtsActivity.setSaldo(saldoFgts.doubleValue());


		if (registerHistory) {
			registerHistory(salary, months);
		}

		this.saldoFgtsActivity.showResultPanels();

	}

	private void registerHistory(Double salary, Integer mes) {
		try {
			History h = new History();
			h.data = new JSONObject();
			if (mes > 0)
				h.data.put(SaldoFgtsDataParser.MESES, mes);
			
			h.type = CalcType.FGTS;
			h.amount = salary;
			historyDAO.saveHistory(h);
	} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void performBtnClearClick() {
		this.saldoFgtsActivity.clearFields();
		this.saldoFgtsActivity.hideResultPanels();
	}

}
