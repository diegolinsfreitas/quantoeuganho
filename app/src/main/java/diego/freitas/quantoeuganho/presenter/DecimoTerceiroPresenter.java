package diego.freitas.quantoeuganho.presenter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.json.JSONException;
import org.json.JSONObject;

import diego.freitas.quantoeuganho.DecimoTerceiroActivity;
import diego.freitas.quantoeuganho.model.CalcType;
import diego.freitas.quantoeuganho.model.DecimoTerceiroDataParser;
import diego.freitas.quantoeuganho.model.History;
import diego.freitas.quantoeuganho.persistence.HistoryDAO;

public class DecimoTerceiroPresenter {

	private DecimoTerceiroActivity decimoTereceiroActivity;
	private HistoryDAO historyDAO;
	private DecimoTerceiroCalculator decimoTerceiroCalculator;

	public DecimoTerceiroPresenter(DecimoTerceiroActivity decimoTereceiroActivity) {
		this.decimoTereceiroActivity = decimoTereceiroActivity;
		historyDAO = new HistoryDAO();
	}

	public void performBtnCalcClick(Double salary,
			Integer dependents, Integer months, int selectedParcela, boolean registerHistory) {
		
		
		BigDecimal salaryForCalc = new BigDecimal(salary);
		decimoTerceiroCalculator = new DecimoTerceiroCalculator();
		
		String[] labels = null;
		double[] data = null;
		if(selectedParcela == 0){
			decimoTerceiroCalculator.calculateParcelaUnica(salaryForCalc, dependents, months);
			
			this.decimoTereceiroActivity
			.setSalaryResult(salaryForCalc.doubleValue());

			this.decimoTereceiroActivity.setInssResult(decimoTerceiroCalculator.inssCalculator.getValue()
					.doubleValue());
			this.decimoTereceiroActivity.setDecimoTerceiro(decimoTerceiroCalculator.decimoTerceiro.doubleValue(), months);
			this.decimoTereceiroActivity.hidePrimeiraParcela();
			this.decimoTereceiroActivity.showCalcView();
			this.decimoTereceiroActivity.setIrrfResult(decimoTerceiroCalculator.irrfCalculator.getValue()
					.doubleValue());
			this.decimoTereceiroActivity.setProventos(decimoTerceiroCalculator.decimoTerceiro.doubleValue());
			this.decimoTereceiroActivity.setDicounts(decimoTerceiroCalculator.discounts.doubleValue());
			decimoTereceiroActivity.setLiquidLabel("Parcela Única");
			this.decimoTereceiroActivity.setLiquid(decimoTerceiroCalculator.parcelaUnica.doubleValue());
			this.decimoTereceiroActivity.setLiquidLabel("Líquido");
			DecimalFormat numberFormat = new DecimalFormat("00.00%");
		
			String inssRate = "";
			if (decimoTerceiroCalculator.inssCalculator.istTetoReached()) {
				inssRate = "Teto";
			} else {
				inssRate = numberFormat.format(decimoTerceiroCalculator.inssCalculator.rate.doubleValue());
			}
			this.decimoTereceiroActivity.setInssRate(inssRate);
		
			String irrfRateText = "";
			if (decimoTerceiroCalculator.irrfCalculator.rate.equals(BigDecimal.ZERO)) {
				irrfRateText = "Isento";
			} else {
				irrfRateText = numberFormat.format(decimoTerceiroCalculator.irrfCalculator.rate
						.doubleValue());
			}
			
		
			this.decimoTereceiroActivity.setIrrfRate(irrfRateText);
			 labels=new String[]{"Única","INSS","IRRF"};
			 data = new double[]{
						decimoTerceiroCalculator.parcelaUnica.doubleValue()/decimoTerceiroCalculator.decimoTerceiro.doubleValue()*100,
						decimoTerceiroCalculator.inssCalculator.value.doubleValue()/decimoTerceiroCalculator.decimoTerceiro.doubleValue()*100,
						decimoTerceiroCalculator.irrfCalculator.value.doubleValue()/decimoTerceiroCalculator.decimoTerceiro.doubleValue()*100,
					};

			
		} else if(selectedParcela == 1){
			decimoTerceiroCalculator.calculatePrimeriaParcela(salaryForCalc, months);
			
			this.decimoTereceiroActivity
			.setSalaryResult(salaryForCalc.doubleValue());
			this.decimoTereceiroActivity.setDecimoTerceiro(decimoTerceiroCalculator.decimoTerceiro.doubleValue(), months);
			this.decimoTereceiroActivity.hidePrimeiraParcela();
			this.decimoTereceiroActivity.setInssResult(null);
			this.decimoTereceiroActivity.setIrrfResult(null);
			this.decimoTereceiroActivity.setIrrfRate("Não aplicável");
			this.decimoTereceiroActivity.setInssRate("Não aplicável");
			this.decimoTereceiroActivity.setLiquid(decimoTerceiroCalculator.primeiraParcela.doubleValue());
			this.decimoTereceiroActivity.setDicounts(null);
			this.decimoTereceiroActivity.setLiquidLabel("1ª Parcela (50%)");
			this.decimoTereceiroActivity.hideCalcView();
			decimoTereceiroActivity.hidePrimeiraParcela();
			labels=new String[]{"Primeira"};
			data = new double[]{
					100
				};
		} else if(selectedParcela == 2){
			decimoTerceiroCalculator.calculateSegundaParcela(salaryForCalc, dependents, months);
			
			this.decimoTereceiroActivity
			.setSalaryResult(salaryForCalc.doubleValue());
			this.decimoTereceiroActivity.setDecimoTerceiro(decimoTerceiroCalculator.decimoTerceiro.doubleValue(), months);

			this.decimoTereceiroActivity.setInssResult(decimoTerceiroCalculator.inssCalculator.getValue()
					.doubleValue());
			this.decimoTereceiroActivity.setIrrfResult(decimoTerceiroCalculator.irrfCalculator.getValue()
					.doubleValue());
			this.decimoTereceiroActivity.setProventos(decimoTerceiroCalculator.decimoTerceiro.doubleValue());
			this.decimoTereceiroActivity.setDicounts(decimoTerceiroCalculator.discounts.doubleValue());
			this.decimoTereceiroActivity.setLiquid(decimoTerceiroCalculator.segundaParcela.doubleValue());
			this.decimoTereceiroActivity.setLiquidLabel("2º Parcela");
			this.decimoTereceiroActivity.showCalcView();
			decimoTereceiroActivity.showPrimeiraParcela();
			decimoTereceiroActivity.setPrimeiraParcela(decimoTerceiroCalculator.primeiraParcela.doubleValue());
			DecimalFormat numberFormat = new DecimalFormat("00.00%");
		
			String inssRate = "";
			if (decimoTerceiroCalculator.inssCalculator.istTetoReached()) {
				inssRate = "Teto";
			} else {
				inssRate = numberFormat.format(decimoTerceiroCalculator.inssCalculator.rate.doubleValue());
			}
			this.decimoTereceiroActivity.setInssRate(inssRate);
		
			String irrfRateText = "";
			if (decimoTerceiroCalculator.irrfCalculator.rate.equals(BigDecimal.ZERO)) {
				irrfRateText = "Isento";
			} else {
				irrfRateText = numberFormat.format(decimoTerceiroCalculator.irrfCalculator.rate
						.doubleValue());
			}
		
			this.decimoTereceiroActivity.setIrrfRate(irrfRateText);
			 labels=new String[]{"2ª parcela","1º parcela", "INSS","IRRF"};
			 data = new double[]{
						decimoTerceiroCalculator.segundaParcela.doubleValue()/decimoTerceiroCalculator.decimoTerceiro.doubleValue()*100,
						decimoTerceiroCalculator.primeiraParcela.doubleValue()/decimoTerceiroCalculator.decimoTerceiro.doubleValue()*100,
						decimoTerceiroCalculator.inssCalculator.value.doubleValue()/decimoTerceiroCalculator.decimoTerceiro.doubleValue()*100,
						decimoTerceiroCalculator.irrfCalculator.value.doubleValue()/decimoTerceiroCalculator.decimoTerceiro.doubleValue()*100,
					};

		}
		
		this.decimoTereceiroActivity.updateGraph(labels, data);

		if (registerHistory) {
			registerHistory(salary, dependents, months, selectedParcela);
		}

		this.decimoTereceiroActivity.showResultPanels();

	}
	
	public static class DecimoTerceiroCalculator {
		
		public BigDecimal primeiraParcela;
		
		InssCalculator inssCalculator = new InssCalculator();
		IRRFCalculator irrfCalculator = new IRRFCalculator();

		public BigDecimal decimoTerceiro;

		public BigDecimal parcelaUnica;

		private BigDecimal discounts;

		private BigDecimal segundaParcela;

		public void calculatePrimeriaParcela(BigDecimal salary, int months){
			calculateDecimo(salary, months);
			primeiraParcela = decimoTerceiro.divide(BigDecimal.valueOf(2d));
		}

		private void calculateDecimo(BigDecimal salary, int months) {
			decimoTerceiro = salary.divide(BigDecimal.valueOf(12),4, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(months));
		}
		
		public void calculateParcelaUnica(BigDecimal salary, long dependents, int months){
			calculateDecimo(salary, months);
			
			inssCalculator.calculate(decimoTerceiro);

			irrfCalculator.calculate(
					decimoTerceiro,
					inssCalculator.getValue(), BigDecimal.valueOf(dependents));
			
			

			discounts = inssCalculator.getValue().add(irrfCalculator.getValue());

			parcelaUnica = decimoTerceiro.subtract(discounts);
		}
		
		public void calculateSegundaParcela(BigDecimal salary, long dependents, int months){
			calculateDecimo(salary, months);
			calculatePrimeriaParcela(salary, months);
			
			inssCalculator.calculate(decimoTerceiro);
			irrfCalculator.calculate(
					decimoTerceiro,
					inssCalculator.getValue(), BigDecimal.valueOf(dependents));
			discounts = inssCalculator.getValue().add(irrfCalculator.getValue()).add(primeiraParcela);
			segundaParcela = decimoTerceiro.subtract(discounts);
		}
	}

	private void registerHistory(Double salary,
			Integer dependents, Integer months, int selectedParcela) {
		try {
			History h = new History();
			h.data = new JSONObject();
			if (dependents > 0)
				h.data.put(DecimoTerceiroDataParser.DEPENDENTS, dependents);
			
			h.data.put(DecimoTerceiroDataParser.MONTHS, months-1);
			
			h.data.put(DecimoTerceiroDataParser.PARCELA,
						selectedParcela);
			h.type = CalcType.DECIMO_TERCEIRO;
			h.amount = salary;
			historyDAO.saveHistory(h);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void performBtnClearClick() {
		this.decimoTereceiroActivity.clearFields();
		this.decimoTereceiroActivity.hideResultPanels();
	}

}
