package diego.freitas.quantoeuganho.presenter;

import java.math.BigDecimal;

class InssCalculator {
	private static final double TETO_PREVIDENCIARIO = 5189.82d;
	private static final BigDecimal TETO_VALUE = BigDecimal.valueOf(570.88);
	BigDecimal value;
	BigDecimal rate;

	public void calculate(BigDecimal salary) {
		if (salary.doubleValue() <= 0) {
			throw new IllegalArgumentException(
					"valor invalido para salario");
		}
		if (salary.doubleValue() <= 1556.94) {
			rate = BigDecimal.valueOf(0.08);
		} else if (salary.doubleValue() >= 1556.95
				&& salary.doubleValue() <= 2594.92) {
			rate = BigDecimal.valueOf(0.09);
		} else if(salary.doubleValue() >= 2594.93
				&& salary.doubleValue() <= TETO_PREVIDENCIARIO){
			rate = BigDecimal.valueOf(0.11);
		}
		if (salary.doubleValue() > TETO_PREVIDENCIARIO) {
			value = TETO_VALUE;
		} else {
			value = salary.multiply(rate);
		}

	}

	public boolean istTetoReached() {
		return value.equals(TETO_VALUE);
	}

	public BigDecimal getValue() {
		return value;
	}

}