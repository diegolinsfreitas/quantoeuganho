package diego.freitas.quantoeuganho.presenter;

import java.math.BigDecimal;

class IRRFCalculator {

	BigDecimal value = BigDecimal.ZERO;
	BigDecimal rate = BigDecimal.ONE;
	private BigDecimal dependentValue = new BigDecimal(189.59);
	private BigDecimal deducao;

	public void calculate(BigDecimal salary, BigDecimal inss,
			BigDecimal dependents) {
		if (salary.doubleValue() <= 0) {
			throw new IllegalArgumentException(
					"valor invalido para salario");
		}
		salary = salary.subtract(inss);
		if (salary.doubleValue() <= 1903.98) {
			rate = BigDecimal.ZERO;
			deducao = BigDecimal.ZERO;
		} else if (salary.doubleValue() >= 1903.99
				&& salary.doubleValue() <= 2826.65) {
			rate = BigDecimal.valueOf(0.075);
			deducao = BigDecimal.valueOf(142.80);
		} else if (salary.doubleValue() >=  2826.66
				&& salary.doubleValue() <=  3751.05) {
			rate = BigDecimal.valueOf(0.15);
			deducao = BigDecimal.valueOf(354.80);
		} else if (salary.doubleValue() >= 3751.06
				&& salary.doubleValue() <= 4664.68) {
			rate = BigDecimal.valueOf(0.225);
			deducao = BigDecimal.valueOf(636.13);
		} else {
			rate = BigDecimal.valueOf(0.275);
			deducao = BigDecimal.valueOf(869.36);
		}

		value = salary
				.subtract(dependentValue.multiply(dependents))
				.multiply(rate).subtract(deducao);
		if(value.doubleValue()< BigDecimal.ZERO.doubleValue()){
			value = BigDecimal.ZERO;
		}
	}

	public BigDecimal getValue() {
		return value;
	}

}